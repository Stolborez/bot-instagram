<div class="panel panel-default">
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="/models"><i class="glyphicon glyphicon-user"></i> Модели</a>
                </li>
                <li>
                    <a href="/tags"><i class="glyphicon glyphicon-tags"></i> Тэги</a>
                </li>
                <li>
                    <a href="/orders"><i class="glyphicon glyphicon-shopping-cart"></i> Заказы <span
                                class="badge">24</span></a>
                </li>

            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
</div>