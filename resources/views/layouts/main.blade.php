<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/dashboard"><img src="{{ elixir('img/Sigma_logo-02.png') }}" style="width: 130px; height:35px; ;" alt="logo"></a>
        </div>
        <!-- /.navbar-header -->
    @include('includes.sidebar')

    <!-- /.navbar-static-side -->
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
        @yield('content')
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
@include('includes.footer')
</body>

</html>
