@extends('layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <table id="tags-table" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Тэг</th>
                        <th>Описание</th>
                        <th>Кнопки</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <button class="add-modal btn btn-info"><i class="glyphicon glyphicon-plus"></i> Добавить запись</button>
        </div>
    </div>
    <!-- /.container-fluid -->
    <!-- TagFormModal -->
    <div id="TagFormModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>

                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="id">ID</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fid" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="fcode">Тег</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fcode">
                            </div>
                        </div>
                        <p class="fcode_error error text-center alert alert-danger hidden"></p>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="ftext">Описание</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="ftext">
                            </div>
                        </div>
                        <p class="ftext_error error text-center alert alert-danger hidden"></p>
                    </form>
                    <div class="deleteContent">
                        Are you Sure you want to delete <span class="dname"></span> ? <span
                                class="hidden did"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn actionBtn">
                            <span id="footer_action_button" class='glyphicon'> </span>
                        </button>
                        <button type="button" class="btn cancel-selected btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Отмена
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- TagFormModal -->
    <script>
        var table;
        // Заполнении таблицы
        $(document).ready(function () {
            table = $('#tags-table').DataTable({
                serverSide: true,
                processing: true,
                ajax: '/get/tags',
                columns: [
                    {data: 'code'},
                    {data: 'text'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                language: {
                    "processing": "Подождите...",
                    "search": "Поиск:",
                    "lengthMenu": "Показать _MENU_ записей",
                    "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "Записи с 0 до 0 из 0 записей",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "infoPostFix": "",
                    "loadingRecords": "Загрузка записей...",
                    "zeroRecords": "Записи отсутствуют.",
                    "emptyTable": "В таблице отсутствуют данные",
                    "paginate": {
                        "first": "Первая",
                        "previous": "Предыдущая",
                        "next": "Следующая",
                        "last": "Последняя"
                    },
                    "aria": {
                        "sortAscending": ": активировать для сортировки столбца по возрастанию",
                        "sortDescending": ": активировать для сортировки столбца по убыванию"
                    }
                }
            });
        });
        // Обработка нажатия кнопки редактировать в таблице
        $(document).on('click', '.edit-modal', function () {
            $('#footer_action_button').text(" Обновить");
            $('#footer_action_button').addClass('glyphicon-check');
            $('#footer_action_button').removeClass('glyphicon-trash');
            $('.actionBtn').addClass('btn-success');
            $('.actionBtn').removeClass('btn-danger');
            $('.actionBtn').removeClass('delete');
            $('.actionBtn').addClass('edit');
            $('.actionBtn').removeClass('add');
            $('.modal-title').text('Редактирование');
            $('.deleteContent').hide();
            $('.form-horizontal').show();
            var stuff = $(this).data('info').split(',');
            fillmodalData(stuff);
            $('.error').addClass('hidden');
            $(this).parent().parent().addClass('selected');
            $('#TagFormModal').modal('show');
        });

        // Обработка нажатия кнопки отмены в модальном окне
        $(document).on('click', '.cancel-selected', function () {
            $('.selected').removeClass('selected');
        });


        $(document).on('click', '.delete-modal', function () {
            $('#footer_action_button').text(" Удалить");
            $('#footer_action_button').removeClass('glyphicon-check');
            $('#footer_action_button').addClass('glyphicon-trash');
            $('.actionBtn').removeClass('btn-success');
            $('.actionBtn').addClass('btn-danger');
            $('.actionBtn').addClass('delete');
            $('.actionBtn').removeClass('edit');
            $('.actionBtn').removeClass('add');
            $('.modal-title').text('Удаление');
            $('.deleteContent').show();
            $('.form-horizontal').hide();
            var stuff = $(this).data('info').split(',');
            $('.did').text(stuff[0]);
            $('.dname').html(stuff[1] + " " + stuff[2]);
            $('#TagFormModal').modal('show');
        });

        function fillmodalData(details) {
            $('#fid').val(details[0]);
            $('#fcode').val(details[1]);
            $('#ftext').val(details[2]);
        };


        $('.modal-footer').on('click', '.edit', function () {
            console.log("Вход в функцию редактирования");
            $.ajax({
                type: 'post',
                url: '/edit/tag',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': $("#fid").val(),
                    'code': $('#fcode').val(),
                    'text': $('#ftext').val()
                },
                error: function (data) {
                    $('.error').addClass('hidden');
                    console.log(data.responseJSON.errors.code);
                    if (data.responseJSON.errors) {
                        if (data.responseJSON.errors.code) {
                            var fcode_error =  $('.fcode_error');
                            fcode_error.removeClass('hidden');
                            fcode_error.text(data.responseJSON.errors.code);
                        }
                        if (data.responseJSON.errors.text) {
                            var ftext_error =  $('.ftext_error');
                            ftext_error.removeClass('hidden');
                            ftext_error.text(data.responseJSON.errors.text);
                        }
                    }
                },
                success: function (data) {
                    $('#TagFormModal').modal('hide');
                    $('.selected')
                        .replaceWith("<tr><td>" + data.code + "</td><td>" + data.text + "</td>" +
                            "<td><button  class='edit-modal btn btn-xs btn-primary' data-info='" + data.id + "," + data.code + "," + data.text + " '><i class='glyphicon glyphicon-edit' ></i> Редактировать</button>" +
                            "<button class='delete-modal btn btn-xs btn-danger' data-info='" + data.id + "," + data.code + "," + data.text + " '><i class='glyphicon glyphicon-trash'></i> Удалить</button></td></tr>");
                }
            });
        });

        $('.modal-footer').on('click', '.delete', function () {
            $.ajax({
                type: 'post',
                url: '/delete/tag',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': $('.did').text()
                },
                success: function (data) {
                    $('#TagFormModal').modal('hide');
                    table.row('.selected').remove().draw(false);
                }
            });
        });

        // Обработка нажатия кнопки добавть в таблице
        $(document).on('click', '.add-modal', function () {
            $('#footer_action_button').text(" Добавть");
            $('#footer_action_button').addClass('glyphicon-check');
            $('#footer_action_button').removeClass('glyphicon-trash');
            $('.actionBtn').addClass('btn-success');
            $('.actionBtn').removeClass('btn-danger');
            $('.actionBtn').removeClass('delete');
            $('.actionBtn').removeClass('edit');
            $('.actionBtn').addClass('add');
            $('.modal-title').text('Добавление новой записи');
            $('.deleteContent').hide();
            $('.form-horizontal').show();
            $('.error').addClass('hidden');
            fillmodalData(['', '', '']);
            $('#TagFormModal').modal('show');
        });

        $('.modal-footer').on('click', '.add', function () {
            console.log("Вход в функцию добавления");
            $.ajax({
                type: 'post',
                url: '/add/tag',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': $("#fid").val(),
                    'code': $('#fcode').val(),
                    'text': $('#ftext').val()
                },
                error: function (data) {
                    if (data.responseJSON.errors) {
                        if (data.responseJSON.errors.fcode) {
                            $('.fcode_error').removeClass('hidden');
                            $('.fcode_error').text("Заполните поле!");
                        }
                        if (data.responseJSON.errors.ftext) {
                            $('.ftext_error').removeClass('hidden');
                            $('.ftext_error').text("Заполните поле!");
                        }
                    }
                },
                success: function (data) {
                    $('#TagFormModal').modal('hide');
                    table.row.add({
                        code: data.code,
                        text: data.text,
                        action: "<button  class='edit-modal btn btn-xs btn-primary' data-info='" + data.id + "," + data.code + "," + data.text + " '><i class='glyphicon glyphicon-edit' ></i> Редактировать</button>" +
                        "<button class='delete-modal btn btn-xs btn-danger' data-info='" + data.id + "," + data.code + "," + data.text + " '><i class='glyphicon glyphicon-trash'></i> Удалить</button>"
                    }).draw(false);
                }
            });
        });


    </script>
@stop

