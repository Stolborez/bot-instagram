@extends('layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <table id="order-table" class="table table-bordered">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Номер заказа</th>
                        <th>Пользователь</th>
                        <th>Статус заказа</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    <!-- TagFormModal -->
    <div id="TagFormModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>

                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="id">ID</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fid" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="fcode">Тег</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fcode">
                            </div>
                        </div>
                        <p class="fcode_error error text-center alert alert-danger hidden"></p>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="ftext">Описание</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="ftext">
                            </div>
                        </div>
                        <p class="ftext_error error text-center alert alert-danger hidden"></p>
                    </form>
                    <div class="deleteContent">
                        Удалить заказ  <span class="dname"></span> ? <span
                                class="hidden did"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn actionBtn">
                            <span id="footer_action_button" class='glyphicon'> </span>
                        </button>
                        <button type="button" class="btn cancel-selected btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Отмена
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- TagFormModal -->
    <script>
        var table;
        // Заполнении таблицы
        $(document).ready(function () {
            table = $('#order-table').DataTable({
                serverSide: true,
                processing: true,
                ajax: '/get/orders',
                columns: [
                    {
                        data: null,
                        className: "details-control",
                        orderable: false,
                        defaultContent: ''
                    },
                    {data: 'code'},
                    {data: 'name'},
                    {data: 'name_stage'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                order: [[1, 'asc']],
                language: {
                    "processing": "Подождите...",
                    "search": "Поиск:",
                    "lengthMenu": "Показать _MENU_ записей",
                    "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "Записи с 0 до 0 из 0 записей",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "infoPostFix": "",
                    "loadingRecords": "Загрузка записей...",
                    "zeroRecords": "Записи отсутствуют.",
                    "emptyTable": "В таблице отсутствуют данные",
                    "paginate": {
                        "first": "Первая",
                        "previous": "Предыдущая",
                        "next": "Следующая",
                        "last": "Последняя"
                    },
                    "aria": {
                        "sortAscending": ": активировать для сортировки столбца по возрастанию",
                        "sortDescending": ": активировать для сортировки столбца по убыванию"
                    }
                }
            });

        });

        /* Formatting function for row details - modify as you need */
        function format(d) {
            //console.log(d);
            return d.link;
        }

        // Add event listener for opening and closing details
        $('#order-table').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });

        // Обработка нажатия кнопки отмены в модальном окне
        $(document).on('click', '.cancel-selected', function () {
            $('.selected').removeClass('selected');
        });


        $(document).on('click', '.delete-modal', function () {
            $('#footer_action_button').text(" Удалить");
            $('#footer_action_button').removeClass('glyphicon-check');
            $('#footer_action_button').addClass('glyphicon-trash');
            $('.actionBtn').removeClass('btn-success');
            $('.actionBtn').addClass('btn-danger');
            $('.actionBtn').addClass('delete');
            $('.actionBtn').removeClass('edit');
            $('.actionBtn').removeClass('add');
            $('.modal-title').text('Удаление');
            $('.deleteContent').show();
            $('.form-horizontal').hide();
            var stuff = $(this).data('info').split(',');
            console.log(stuff);
            $('.did').text(stuff[0]);
            $('.dname').html("№ " + stuff[1]);
            $(this).parent().parent().addClass('selected');
            $('#TagFormModal').modal('show');
        });


        $('.modal-footer').on('click', '.delete', function () {
            $.ajax({
                type: 'post',
                url: '/delete/order',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': $('.did').text()
                },
                success: function (data) {
                    $('#TagFormModal').modal('hide');
                    table.row('.selected').remove().draw(false);
                }
            });
        });
    </script>
@stop

