@extends('layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive text-center">
                    <table id="models-table" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Фото</th>
                            <th>Фамилия</th>
                            <th>Имя</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <button class="add-modal btn btn-info"><i class="glyphicon glyphicon-plus"></i> Добавить запись</button>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
        <!-- ModelFormModal -->
        <div id="ModelFormModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"></h4>

                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="container-fluid shadow">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#tab1" role="tab" data-toggle="tab">Информация</a></li>
                                    <li role="presentation"><a href="#tab2" role="tab" data-toggle="tab">Фото</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="tab1">
                                        <div class="row" style="padding-top: 20px">

                                            <input id="fid" class="form-control hidden"/>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-1">
                                                        </div>
                                                        <div class="col-md-3 col-xs-12">
                                                            <div class="form-group">
                                                                <img id="fimageLowResolutionUrl" src=""
                                                                     class="img-rounded img-responsive"
                                                                     style="width:185px; height: 185px" alt="photo">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <label class="control-label control-label-left col-sm-3"
                                                                       for="fsurname">Фамилия</label>
                                                                <div class="controls col-sm-9">
                                                                    <input id="fsurname" class="form-control">
                                                                    <span id="surname_error" class="error"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label control-label-left col-sm-3"
                                                                       for="fname">Имя</label>
                                                                <div class="controls col-sm-9">
                                                                    <input id="fname" class="form-control">
                                                                    <span id="name_error" class="error"></span>
                                                                </div>

                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label control-label-left col-sm-3"
                                                                       for="fpatronymic">Отчество</label>
                                                                <div class="controls col-sm-9">
                                                                    <input id="fpatronymic" type="text" class="form-control">
                                                                    <span id="patronymic_error" class="error"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label control-label-left col-sm-3"
                                                                       for="fdateofbirth">Дата рождения</label>
                                                                <div class="controls col-sm-8">
                                                                    <div class="input-group date" id="datetimepicker">
                                                                        <input id="fdateofbirth" type="text"
                                                                               class="form-control" readonly>
                                                                        <span class="input-group-addon"><i
                                                                                    class="glyphicon glyphicon-calendar"></i></span>
                                                                    </div>
                                                                    <span id="dateofbirth_error" class="error"></span>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-6"
                                                               for="fwaistmeasurement">Объем талии</label>
                                                        <div class="controls col-sm-6">
                                                            <input id="fwaistmeasurement" type="text" class="form-control">
                                                            <span id="waistmeasurement_error" class="error"></span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-6"
                                                               for="fbustvolume">Объем бюста</label>
                                                        <div class="controls col-sm-6">

                                                            <input id="fbustvolume" type="text" class="form-control"/>
                                                            <span id="bustvolume_error" class="error"></span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-6"
                                                               for="fvolumehips">Объем бедер</label>
                                                        <div class="controls col-sm-6">

                                                            <input id="fvolumehips" type="text" class="form-control">
                                                            <span id="volumehips_error" class="error"></span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-6"
                                                               for="fhaircolour">Цвет волос</label>
                                                        <div class="controls col-sm-6">
                                                            <input id="fhaircolour" type="text" class="form-control">
                                                            <span id="haircolour_error" class="error"></span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-6"
                                                               for="ffootwearsize">Размер обуви</label>
                                                        <div class="controls col-sm-6">
                                                            <select class="form-control" id="ffootwearsize">
                                                                @for ($i = 32; $i < 43; $i++)
                                                                    <option>{{ $i }}</option>
                                                                @endfor
                                                            </select>
                                                            <span id="footwearsize_error" class="error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-6"
                                                               for="fclothingsize">Размер одежды</label>
                                                        <div class="controls col-sm-6">
                                                            <select id="fclothingsize" class="form-control">
                                                                <option value="XXS">XXS</option>
                                                                <option value="XS">XS</option>
                                                                <option value="S">S</option>
                                                                <option value="L">L</option>
                                                                <option value="M">M</option>
                                                            </select>
                                                            <span id="clothingsize_error" class="error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-6" for="fheight">Рост
                                                            (см)</label>
                                                        <div class="controls col-sm-6">
                                                            <input id="fheight" type="text" class="form-control">
                                                            <span id="height_error" class="error"></span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-6"
                                                               for="fcoloreyes">Цвет глаз</label>
                                                        <div class="controls col-sm-6">
                                                            <input id="fcoloreyes" type="text" class="form-control">
                                                            <span id="coloreyes_error" class="error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-left: 10px; margin-right: 10px">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                    <span class="input-group-addon"><input type="checkbox"
                                                                                           id="fpassport"></span>
                                                            <label type="text" class="form-control">Наличие
                                                                загранпаспорта</label>
                                                        </div><!-- /input-group -->
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <div class="input-group">
                                                    <span class="input-group-addon"><input type="checkbox"
                                                                                           id="fdeparture"></span>
                                                            <label type="text" class="form-control">Готовность к выезду за
                                                                рубеж</label>
                                                        </div><!-- /input-group -->
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><input type="checkbox" id="fvideo"></span>
                                                            <label type="text" class="form-control">Видео-визитка на англ
                                                                яз.</label>
                                                        </div><!-- /input-group -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="control-label control-label-left col-sm-3"
                                                           for="fworkexperience">Опыт работы</label>
                                                    <div class="controls col-sm-8">
                                                        <textarea id="fworkexperience" rows="3" class="form-control"></textarea>
                                                        <span id="workexperience_error" class="error"></span>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="control-label control-label-left col-sm-3" for="fdescription">Краткое
                                                        описание</label>
                                                    <div class="controls col-sm-8">
                                                        <textarea id="fdescription" rows="3" class="form-control"></textarea>
                                                        <span id="description_error" class="error"></span>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="control-label control-label-left col-sm-3"
                                                           for="flinkInstagram">Ссылка инстаграмм</label>
                                                    <div class="controls col-sm-8">
                                                        <input id="flinkInstagram" type="text" class="form-control">
                                                        <span id="linkInstagram_error" class="error"></span>
                                                    </div>

                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label control-label-left col-sm-3" for="flinkvk">Ссылка
                                                        ВК</label>
                                                    <div class="controls col-sm-8">
                                                        <input id="flinkvk" type="text" class="form-control">
                                                        <span id="linkvk_error" class="error"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="control-label control-label-left col-sm-3"
                                                           for="chosen-select ">Теги</label>
                                                    <div class="controls col-sm-8">
                                                        <select class="chosen-container-multi chosen-choices chosen-select form-control"
                                                                multiple="" data-placeholder="Введите теги">
                                                            @foreach ($tags as $tag)
                                                                <option value="{{ $tag->id }}">{{ $tag->text }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="tab2">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="mb-3 dm-uploader" id="drag-and-drop-zone1">
                                                        <input type="hidden" name="filename">
                                                        <div class="form-row">
                                                            <div class="col-md-12  d-md-block  d-sm-none">
                                                                <img src="img/no_image.png" alt="..." class="img-thumbnail">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-md-12 col-sm-12">
                                                                <div class="from-group mb-2">
                                                                    <div class="progress mb-2 d-none">
                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0">0%</div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div role="button" class="btn btn-primary mr-2">
                                                                        <i class="fa fa-folder-o fa-fw"></i> Выберите фото
                                                                        <input type="file" title="Кликните, чтобы добавить Файлы">
                                                                    </div>
                                                                    <small class="status text-muted">Выберите файл или перетащите его по этой область..</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="mb-3 dm-uploader" id="drag-and-drop-zone2">
                                                        <input type="hidden" name="filename">
                                                        <div class="form-row">
                                                            <div class="col-md-12  d-md-block  d-sm-none">
                                                                <img src="https://danielmg.org/assets/image/noimage.jpg?v=v10" alt="..." class="img-thumbnail">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-md-12 col-sm-12">
                                                                <div class="from-group mb-2">
                                                                    <div class="progress mb-2 d-none">
                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0">0%</div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div role="button" class="btn btn-primary mr-2">
                                                                        <i class="fa fa-folder-o fa-fw"></i> Выберите фото
                                                                        <input type="file" title="Кликните, чтобы добавить Файлы">
                                                                    </div>
                                                                    <small class="status text-muted">Выберите файл или перетащите его по этой область..</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="mb-3 dm-uploader" id="drag-and-drop-zone3">
                                                        <input type="hidden" name="filename">
                                                        <div class="form-row">
                                                            <div class="col-md-12  d-md-block  d-sm-none">
                                                                <img src="https://danielmg.org/assets/image/noimage.jpg?v=v10" alt="..." class="img-thumbnail">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-md-12 col-sm-12">
                                                                <div class="from-group mb-2">
                                                                    <div class="progress mb-2 d-none">
                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0">0%</div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div role="button" class="btn btn-primary mr-2">
                                                                        <i class="fa fa-folder-o fa-fw"></i> Выберите фото
                                                                        <input type="file" title="Кликните, чтобы добавить Файлы">
                                                                    </div>
                                                                    <small class="status text-muted">Выберите файл или перетащите его по этой область..</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="mb-3 dm-uploader" id="drag-and-drop-zone4">
                                                        <input type="hidden" name="filename">
                                                        <div class="form-row">
                                                            <div class="col-md-12  d-md-block  d-sm-none">
                                                                <img src="https://danielmg.org/assets/image/noimage.jpg?v=v10" alt="..." class="img-thumbnail">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-md-12 col-sm-12">
                                                                <div class="from-group mb-2">
                                                                    <div class="progress mb-2 d-none">
                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0">0%</div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div role="button" class="btn btn-primary mr-2">
                                                                        <i class="fa fa-folder-o fa-fw"></i> Выберите фото
                                                                        <input type="file" title="Кликните, чтобы добавить Файлы">
                                                                    </div>
                                                                    <small class="status text-muted">Выберите файл или перетащите его по этой область..</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="mb-3 dm-uploader" id="drag-and-drop-zone5">
                                                        <input type="hidden" name="filename">
                                                        <div class="form-row">
                                                            <div class="col-md-12  d-md-block  d-sm-none">
                                                                <img src="https://danielmg.org/assets/image/noimage.jpg?v=v10" alt="..." class="img-thumbnail">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-md-12 col-sm-12">
                                                                <div class="from-group mb-2">
                                                                    <div class="progress mb-2 d-none">
                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0">0%</div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div role="button" class="btn btn-primary mr-2">
                                                                        <i class="fa fa-folder-o fa-fw"></i> Выберите фото
                                                                        <input type="file" title="Кликните, чтобы добавить Файлы">
                                                                    </div>
                                                                    <small class="status text-muted">Выберите файл или перетащите его по этой область..</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="mb-3 dm-uploader" id="drag-and-drop-zone6">
                                                        <input type="hidden" name="filename">
                                                        <div class="form-row">
                                                            <div class="col-md-12  d-md-block  d-sm-none">
                                                                <img src="https://danielmg.org/assets/image/noimage.jpg?v=v10" alt="..." class="img-thumbnail">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-md-12 col-sm-12">
                                                                <div class="from-group mb-2">
                                                                    <div class="progress mb-2 d-none">
                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0">0%</div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div role="button" class="btn btn-primary mr-2">
                                                                        <i class="fa fa-folder-o fa-fw"></i> Выберите фото
                                                                        <input type="file" title="Кликните, чтобы добавить Файлы">
                                                                    </div>
                                                                    <small class="status text-muted">Выберите файл или перетащите его по этой область..</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="mb-3 dm-uploader" id="drag-and-drop-zone7">
                                                        <input type="hidden" name="filename">
                                                        <div class="form-row">
                                                            <div class="col-md-12  d-md-block  d-sm-none">
                                                                <img src="https://danielmg.org/assets/image/noimage.jpg?v=v10" alt="..." class="img-thumbnail">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-md-12 col-sm-12">
                                                                <div class="from-group mb-2">
                                                                    <div class="progress mb-2 d-none">
                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0">0%</div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div role="button" class="btn btn-primary mr-2">
                                                                        <i class="fa fa-folder-o fa-fw"></i> Выберите фото
                                                                        <input type="file" title="Кликните, чтобы добавить Файлы">
                                                                    </div>
                                                                    <small class="status text-muted">Выберите файл или перетащите его по этой область..</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="mb-3 dm-uploader" id="drag-and-drop-zone8">
                                                        <input type="hidden" name="filename">
                                                        <div class="form-row">
                                                            <div class="col-md-12  d-md-block  d-sm-none">
                                                                <img src="https://danielmg.org/assets/image/noimage.jpg?v=v10" alt="..." class="img-thumbnail">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-md-12 col-sm-12">
                                                                <div class="from-group mb-2">
                                                                    <div class="progress mb-2 d-none">
                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0">0%</div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div role="button" class="btn btn-primary mr-2">
                                                                        <i class="fa fa-folder-o fa-fw"></i> Выберите фото
                                                                        <input type="file" title="Кликните, чтобы добавить Файлы">
                                                                    </div>
                                                                    <small class="status text-muted">Выберите файл или перетащите его по этой область..</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="mb-3 dm-uploader" id="drag-and-drop-zone9">
                                                        <input type="hidden" name="filename">
                                                        <div class="form-row">
                                                            <div class="col-md-12  d-md-block  d-sm-none">
                                                                <img src="https://danielmg.org/assets/image/noimage.jpg?v=v10" alt="..." class="img-thumbnail">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-md-12 col-sm-12">
                                                                <div class="from-group mb-2">
                                                                    <div class="progress mb-2 d-none">
                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0">0%</div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div role="button" class="btn btn-primary mr-2">
                                                                        <i class="fa fa-folder-o fa-fw"></i> Выберите фото
                                                                        <input type="file" title="Кликните, чтобы добавить Файлы">
                                                                    </div>
                                                                    <small class="status text-muted">Выберите файл или перетащите его по этой область..</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="mb-3 dm-uploader" id="drag-and-drop-zone10">
                                                        <input type="hidden" name="filename">
                                                        <div class="form-row">
                                                            <div class="col-md-12  d-md-block  d-sm-none">
                                                                <img src="https://danielmg.org/assets/image/noimage.jpg?v=v10" alt="..." class="img-thumbnail">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-md-12 col-sm-12">
                                                                <div class="from-group mb-2">
                                                                    <div class="progress mb-2 d-none">
                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0">0%</div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div role="button" class="btn btn-primary mr-2">
                                                                        <i class="fa fa-folder-o fa-fw"></i> Выберите фото
                                                                        <input type="file" title="Кликните, чтобы добавить Файлы">
                                                                    </div>
                                                                    <small class="status text-muted">Выберите файл или перетащите его по этой область..</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="deleteContent">
                        Удалить запись <span class="dname"></span> ? <span
                                class="hidden did"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn actionBtn">
                            <span id="footer_action_button" class='glyphicon'> </span>
                        </button>
                        <button type="button" class="btn cancel-selected btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Отмена
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ModelFormModal -->

    <script type="text/javascript">

        var table;
        var footer_action_button = $('#footer_action_button');
        var actionBtn = $('.actionBtn');

        $(document).ready(function () {
            table = $('#models-table').DataTable({
                serverSide: true,
                processing: true,
                ajax: '/get/models',
                columns: [
                    {
                        data: 'photos', name: 'photos',
                        render: function (data, type, full, meta) {
                            return "<img src=\"" + data + "\" height=\"50\"/>";
                        }, orderable: false, searchable: false
                    },
                    {data: 'surname'},
                    {data: 'name'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                language: {
                    "processing": "Подождите...",
                    "search": "Поиск:",
                    "lengthMenu": "Показать _MENU_ записей",
                    "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "Записи с 0 до 0 из 0 записей",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "infoPostFix": "",
                    "loadingRecords": "Загрузка записей...",
                    "zeroRecords": "Записи отсутствуют.",
                    "emptyTable": "В таблице отсутствуют данные",
                    "paginate": {
                        "first": "Первая",
                        "previous": "Предыдущая",
                        "next": "Следующая",
                        "last": "Последняя"
                    },
                    "aria": {
                        "sortAscending": ": активировать для сортировки столбца по возрастанию",
                        "sortDescending": ": активировать для сортировки столбца по убыванию"
                    }
                }
            });

        });

        $(document).ready(function () {
            $("#datetimepicker").datetimepicker({
                format: 'DD.MM.YYYY',
                locale: 'ru',
                ignoreReadonly: true,
                useCurrent: false
            });
            $(".chosen-select").chosen({width: "100%"});
        });

        /**
         * Обработка нажатия кнопки редактировать в таблице
         */
        $(document).on('click', '.edit-modal', function () {
            footer_action_button.text(" Обновить");
            footer_action_button.addClass('glyphicon-check');
            footer_action_button.removeClass('glyphicon-trash');
            actionBtn.addClass('btn-success');
            actionBtn.removeClass('btn-danger');
            actionBtn.removeClass('delete');
            actionBtn.addClass('edit');
            actionBtn.removeClass('add');
            $('.modal-title').text('Редактирование');
            $('.deleteContent').hide();
            $('.form-horizontal').show();
            $('#fid').val($(this).data('info'));
            var result = null;
            $.ajax({
                type: 'post',
                url: '/get/model/' + $(this).data('info'),
                data: {
                    '_token': $('input[name=_token]').val()
                },
                error: function (data) {
                    if (data.responseJSON.errors) {
                        alert("Ошибка получения данных модели");
                    }
                },
                success: function (data) {
                    result = $.parseJSON(data);
                    fillmodalData(result);
                }
            });
            $('.error').addClass('hidden');
            $(this).parent().parent().addClass('selected');
            $('#ModelFormModal').modal('show');
        });

        /**
         * Заполнение полей формы из полученного массива
         * @param details Массив данных для запонения полей
         */
        function fillmodalData(details) {
            $('#fsurname').val(details["0"].surname);
            $('#fname').val(details["0"].name);
            $('#fpatronymic').val(details["0"].patronymic);
            if (details["0"].dateofbirth && details["0"].dateofbirth.length !== 0) {
                $('#fdateofbirth').val(moment(details["0"].dateofbirth).format('DD.MM.YYYY'));
            } else {
                $('#fdateofbirth').val("");
            }
            $('#fheight').val(details["0"].height);
            $('#fcoloreyes').val(details["0"].coloreyes);
            $('#fwaistmeasurement').val(details["0"].waistmeasurement);
            $('#fbustvolume').val(details["0"].bustvolume);
            $('#fvolumehips').val(details["0"].volumehips);
            $('#fhaircolour').val(details["0"].haircolour);
            $('#ffootwearsize').val(details["0"].footwearsize);
            $('#fclothingsize').val(details["0"].clothingsize);
            $('#fpassport').prop('checked', details["0"].passport === 1);
            $('#fdeparture').prop('checked', details["0"].departure === 1);
            $("#fvideo").prop('checked', details["0"].video === 1);
            $('#fworkexperience').val(details["0"].workexperience);
            $('#fdescription').val(details["0"].description);
            $('#flinkInstagram').val(details["0"].linkInstagram);
            $('#flinkvk').val(details["0"].linkvk);
            $('#fimageLowResolutionUrl').attr("src", details["0"].imageLowResolutionUrl);
            $('#drag-and-drop-zone1').children('input').val("");
            $('#drag-and-drop-zone2').children('input').val("");
            $('#drag-and-drop-zone3').children('input').val("");
            $('#drag-and-drop-zone4').children('input').val("");
            $('#drag-and-drop-zone5').children('input').val("");
            $('#drag-and-drop-zone6').children('input').val("");
            $('#drag-and-drop-zone7').children('input').val("");
            $('#drag-and-drop-zone8').children('input').val("");
            $('#drag-and-drop-zone9').children('input').val("");
            $('#drag-and-drop-zone10').children('input').val("");
            if (details["0"].photo_main === null) {
                $('#drag-and-drop-zone1').find('img').attr("src", "/img/no_image.png");
            }
            else {
                $('#drag-and-drop-zone1').find('img').attr("src", details["0"].photo_main);
            }
            if (details["0"].photo_1 === null) {
                $('#drag-and-drop-zone2').find('img').attr("src", "/img/no_image.png");
            }
            else {
                $('#drag-and-drop-zone2').find('img').attr("src", details["0"].photo_1);
            }
            if (details["0"].photo_2 === null) {
                $('#drag-and-drop-zone3').find('img').attr("src", "/img/no_image.png");
                console.log("пусто");
            }
            else {
                console.log(details["0"].photo_2);
                $('#drag-and-drop-zone3').find('img').attr("src", details["0"].photo_2);
            }
            if (details["0"].photo_3 === null) {
                $('#drag-and-drop-zone4').find('img').attr("src", "img/no_image.png");
            }
            else {
                $('#drag-and-drop-zone4').find('img').attr("src", details["0"].photo_3);
            }
            if (details["0"].photo_4 === null) {
                $('#drag-and-drop-zone5').find('img').attr("src", "img/no_image.png");
            }
            else {
                $('#drag-and-drop-zone5').find('img').attr("src", details["0"].photo_4);
            }
            if (details["0"].photo_5 === null) {
                $('#drag-and-drop-zone6').find('img').attr("src", "img/no_image.png");
            }
            else {
                $('#drag-and-drop-zone6').find('img').attr("src", details["0"].photo_5);
            }
            if (details["0"].photo_6 === null) {
                $('#drag-and-drop-zone7').find('img').attr("src", "img/no_image.png");
            }
            else {
                $('#drag-and-drop-zone7').find('img').attr("src", details["0"].photo_6);
            }
            if (details["0"].photo_7 === null) {
                $('#drag-and-drop-zone8').find('img').attr("src", "img/no_image.png");
            }
            else {
                $('#drag-and-drop-zone8').find('img').attr("src", details["0"].photo_7);
            }
            if (details["0"].photo_8 === null) {
                $('#drag-and-drop-zone9').find('img').attr("src", "img/no_image.png");
            }
            else {
                $('#drag-and-drop-zone9').find('img').attr("src", details["0"].photo_8);
            }
            if (details["0"].photo_9 === null) {
                $('#drag-and-drop-zone10').find('img').attr("src", "/img/no_image.png");
            }
            else {
                $('#drag-and-drop-zone10').find('img').attr("src", details["0"].photo_9);
            }
            var tags = [];
            for (var item in details["tags"]) {
                tags.push(details["tags"][item].tag_id);
            }
            $('.chosen-select').val(tags).trigger("chosen:updated");


        }

        /**
         * Обработка нажатия кнопки отмены в модальном окне
         */
        $(document).on('click', '.cancel-selected', function () {
            $('.selected').removeClass('selected');
        });

        /**
         * Обработка нажатия крестика(закрытия) в модальном окне
         */
        $(document).on('click', 'button.close', function () {
            $('.selected').removeClass('selected');
        });

        /**
         * Обработка нажатия кнопки удалить в таблице
         */
        $(document).on('click', '.delete-modal', function () {
            footer_action_button.text(" Удалить");
            footer_action_button.removeClass('glyphicon-check');
            footer_action_button.addClass('glyphicon-trash');
            actionBtn.removeClass('btn-success');
            actionBtn.addClass('btn-danger');
            actionBtn.addClass('delete');
            actionBtn.removeClass('edit');
            actionBtn.removeClass('add');
            $('.modal-title').text('Удаление');
            $('.deleteContent').show();
            $('.form-horizontal').hide();
            var stuff = $(this).data('info').split(',');
            $('.did').text(stuff[0]);
            $('.dname').html(stuff[1] + " " + stuff[2]);
            $(this).parent().parent().addClass('selected');
            $('#ModelFormModal').modal('show');
        });

        /**
         * Обработка нажатия кнопки добавть в таблице
         */
        $(document).on('click', '.add-modal', function () {
            footer_action_button.text(" Добавть");
            footer_action_button.addClass('glyphicon-check');
            footer_action_button.removeClass('glyphicon-trash');
            actionBtn.addClass('btn-success');
            actionBtn.removeClass('btn-danger');
            actionBtn.removeClass('delete');
            actionBtn.removeClass('edit');
            actionBtn.addClass('add');
            $('.modal-title').text('Добавление новой записи');
            $('.deleteContent').hide();
            $('.form-horizontal').show();
            $('.error').addClass('hidden');
            $('#ModelFormModal').modal('show');
        });

        /**
         * Ajax запрос редактирования модели
         */
        $('.modal-footer').on('click', '.edit', function () {
            $.ajax({
                type: 'post',
                url: '/edit/model',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'surname': $('#fsurname').val(),
                    'name': $('#fname').val(),
                    'patronymic': $('#fpatronymic').val(),
                    'dateofbirth': $('#fdateofbirth').val(),
                    'height': $('#fheight').val(),
                    'coloreyes': $('#fcoloreyes').val(),
                    'weight': $('#fweight').val(),
                    'waistmeasurement': $('#fwaistmeasurement').val(),
                    'bustvolume': $('#fbustvolume').val(),
                    'volumehips': $('#fvolumehips').val(),
                    'haircolour': $('#fhaircolour').val(),
                    'footwearsize': $('#ffootwearsize').val(),
                    'clothingsize': $('#fclothingsize').val(),
                    'passport': $('#fpassport').is(':checked') ? 1 : 0,
                    'departure': $('#fdeparture').is(':checked') ? 1 : 0,
                    'video': $("#video").is(':checked') ? 1 : 0,
                    'workexperience': $('#fworkexperience').val(),
                    'description': $('#fdescription').val(),
                    'linkInstagram': $('#flinkInstagram').val(),
                    'linkvk': $('#flinkvk').val(),
                    'imageLowResolutionUrl': $('#fimageLowResolutionUrl').attr("src"),
                    'id': $('#fid').val(),
                    'tags': $(".chosen-select").val(),
                    'photo_main': $('#drag-and-drop-zone1').children('input').val(),
                    'photo_1': $('#drag-and-drop-zone2').children('input').val(),
                    'photo_2': $('#drag-and-drop-zone3').children('input').val(),
                    'photo_3': $('#drag-and-drop-zone4').children('input').val(),
                    'photo_4': $('#drag-and-drop-zone5').children('input').val(),
                    'photo_5': $('#drag-and-drop-zone6').children('input').val(),
                    'photo_6': $('#drag-and-drop-zone7').children('input').val(),
                    'photo_7': $('#drag-and-drop-zone8').children('input').val(),
                    'photo_8': $('#drag-and-drop-zone9').children('input').val(),
                    'photo_9': $('#drag-and-drop-zone10').children('input').val()
                },
                error: function (data) {
                    $('.error').addClass('hidden');
                    if (data.responseJSON.errors) {
                        if (data.responseJSON.errors.surname) {
                            var surname_error = $('#surname_error');
                            surname_error.removeClass('hidden');
                            surname_error.text(data.responseJSON.errors.surname);
                        }
                        if (data.responseJSON.errors.name) {
                            var name_error = $('#name_error');
                            name_error.removeClass('hidden');
                            name_error.text(data.responseJSON.errors.name);
                        }
                        if (data.responseJSON.errors.patronymic) {
                            var patronymic_error = $('#patronymic_error');
                            patronymic_error.removeClass('hidden');
                            patronymic_error.text(data.responseJSON.errors.patronymic);
                        }
                        if (data.responseJSON.errors.height) {
                            var height_error = $('#height_error');
                            height_error.removeClass('hidden');
                            height_error.text(data.responseJSON.errors.height);
                        }
                        if (data.responseJSON.errors.coloreyes) {
                            var coloreyes_error = $('#coloreyes_error');
                            coloreyes_error.removeClass('hidden');
                            coloreyes_error.text(data.responseJSON.errors.coloreyes);
                        }
                        if (data.responseJSON.errors.weight) {
                            var weight_error = $('#weight_error');
                            weight_error.removeClass('hidden');
                            weight_error.text(data.responseJSON.errors.weight);
                        }
                        if (data.responseJSON.errors.waistmeasurement) {
                            var waistmeasurement_error = $('#waistmeasurement_error');
                            waistmeasurement_error.removeClass('hidden');
                            waistmeasurement_error.text(data.responseJSON.errors.waistmeasurement);
                        }
                        if (data.responseJSON.errors.bustvolume) {
                            var bustvolume_error = $('#bustvolume_error');
                            bustvolume_error.removeClass('hidden');
                            bustvolume_error.text(data.responseJSON.errors.bustvolume);
                        }
                        if (data.responseJSON.errors.volumehips) {
                            var volumehips_error = $('#volumehips_error');
                            volumehips_error.removeClass('hidden');
                            volumehips_error.text(data.responseJSON.errors.volumehips);
                        }
                        if (data.responseJSON.errors.haircolour) {
                            var haircolour_error = $('#haircolour_error');
                            haircolour_error.removeClass('hidden');
                            haircolour_error.text(data.responseJSON.errors.haircolour);
                        }
                        if (data.responseJSON.errors.workexperience) {
                            var workexperience_error = $('#workexperience_error');
                            workexperience_error.removeClass('hidden');
                            workexperience_error.text(data.responseJSON.errors.workexperience);
                        }
                        if (data.responseJSON.errors.linkInstagram) {
                            var linkInstagram_error = $('#linkInstagram_error');
                            linkInstagram_error.removeClass('hidden');
                            linkInstagram_error.text(data.responseJSON.errors.linkInstagram);
                        }
                        if (data.responseJSON.errors.linkvk) {
                            var linkvk_error = $('#linkvk_error');
                            linkvk_error.removeClass('hidden');
                            linkvk_error.text(data.responseJSON.errors.linkvk);
                        }
                        if (data.responseJSON.errors.footwearsize) {
                            var footwearsize_error = $('#footwearsize_error');
                            footwearsize_error.removeClass('hidden');
                            footwearsize_error.text(data.responseJSON.errors.footwearsize);
                        }
                        if (data.responseJSON.errors.clothingsize) {
                            var clothingsize_error = $('#clothingsize_error');
                            clothingsize_error.removeClass('hidden');
                            clothingsize_error.text(data.responseJSON.errors.clothingsize);
                        }
                        if (data.responseJSON.errors.dateofbirth) {
                            var dateofbirth_error = $('#dateofbirth_error');
                            dateofbirth_error.removeClass('hidden');
                            dateofbirth_error.text(data.responseJSON.errors.dateofbirth);
                        }
                    }
                },
                success: function (data) {
                    $('#ModelFormModal').modal('hide');
                    $('.selected')
                        .replaceWith("<tr role='row'><td><img src='" + data.imageLowResolutionUrl + "' height='50'></td><td>" + data.surname + "</td><td>" + data.name + "</td><td>" +
                            "<button class='edit-modal btn btn-xs btn-primary' data-info='" + data.id + "'><i class='glyphicon glyphicon-edit'></i>Редактировать</button>" +
                            "<button class='delete-modal btn btn-xs btn-danger' data-info='" + data.id + "," + data.name + "," + data.surname + "'><i class='glyphicon glyphicon-remove'></i>Удалить</button>" +
                            "<br><a href='https://www.instagram.com/p/'" + data.code + "' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-'></i>Instagram</a></td></tr>");
                }
            });
        });

        $('.modal-footer').on('click', '.delete', function () {
            $.ajax({
                type: 'post',
                url: '/del/model/' + $('.did').text(),
                data: {
                    '_token': $('input[name=_token]').val()
                },
                success: function (data) {
                    $('#ModelFormModal').modal('hide');
                    table.row('.selected').remove().draw(false);
                }
            });
        });


        // Adds an entry to our debug area
        function ui_add_log(message) {
            var d = new Date();
            var dateString = (('0' + d.getHours())).slice(-2) + ':' +
                (('0' + d.getMinutes())).slice(-2) + ':' +
                (('0' + d.getSeconds())).slice(-2);
            console.log("Дата:" + dateString + " Сообщение:" + message);
        }

        function ui_single_update_active(element, active) {
            element.find('div.progress').toggleClass('d-none', !active);
            element.find('input[type="text"]').toggleClass('d-none', active);

            element.find('input[type="file"]').prop('disabled', active);
            element.find('.btn').toggleClass('disabled', active);

            element.find('.btn i').toggleClass('fa-circle-o-notch fa-spin', active);
            element.find('.btn i').toggleClass('fa-folder-o', !active);
        }

        function ui_single_update_progress(element, percent, active) {
            active = (typeof active === 'undefined' ? true : active);

            var bar = element.find('div.progress-bar');

            bar.width(percent + '%').attr('aria-valuenow', percent);
            bar.toggleClass('progress-bar-striped progress-bar-animated', active);

            if (percent === 0) {
                bar.html('');
            } else {
                bar.html(percent + '%');
            }
        }

        function ui_single_update_status(element, message, color) {
            color = (typeof color === 'undefined' ? 'muted' : color);

            element.find('small.status').prop('class', 'status text-' + color).html(message);
        }

        function setInitdmUploader(element) {
            /*
             * For the sake keeping the code clean and the examples simple this file
             * contains only the plugin configuration & callbacks.
             *
             * UI functions ui_* can be located in:
             *   - assets/demo/uploader/js/ui-main.js
             *   - assets/demo/uploader/js/ui-multiple.js
             *   - assets/demo/uploader/js/ui-single.js
             */
            element.dmUploader({
                url: '/model/photos',
                maxFileSize: 10000000,
                multiple: false,
                allowedTypes: 'image/*',
                extraData: function () {
                    return {
                        "_token": $('input[name=_token]').val()
                    };
                },
                extFilter: ['jpg', 'jpeg', 'png', 'gif'],
                onDragEnter: function () {
                    // Happens when dragging something over the DnD area
                    this.addClass('active');
                },
                onDragLeave: function () {
                    // Happens when dragging something OUT of the DnD area
                    this.removeClass('active');
                },
                onInit: function () {
                    // Plugin is ready to use
                    ui_add_log('Плагин инициализирован');

                    this.find('input[type="text"]').val('');
                },
                onComplete: function () {
                    // All files in the queue are processed (success or error)

                    ui_add_log('All pending tranfers finished');
                },
                onNewFile: function (id, file) {
                    // When a new file is added using the file selector or the DnD area
                    ui_add_log('New file added #' + id);

                    if (typeof FileReader !== "undefined") {
                        var reader = new FileReader();
                        var img = this.find('img');

                        reader.onload = function (e) {
                            img.attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);
                    }
                },
                onBeforeUpload: function (id) {
                    // about tho start uploading a file
                    ui_add_log('Начата загрузка #' + id);
                    ui_single_update_progress(this, 0, true);
                    ui_single_update_active(this, true);

                    ui_single_update_status(this, 'Загрузка...');
                },
                onUploadProgress: function (id, percent) {
                    // Updating file progress
                    ui_single_update_progress(this, percent);
                },
                onUploadSuccess: function (id, data) {
                    var json_parse = JSON.parse(data);
                    $(element).children('input').attr('value', json_parse.path);
                    // A file was successfully uploaded
                    ui_add_log('Ответ сервера для файла #' + id + ': ' + data);
                    ui_add_log('Загрузка файла #' + id + ' успешна');

                    ui_single_update_active(this, false);

                    ui_single_update_status(this, 'Загрузка успешна.', 'success');
                },
                onUploadError: function (id, xhr, status, message) {
                    // Happens when an upload error happens
                    ui_single_update_active(this, false);
                    ui_single_update_status(this, 'Ошибка: ' + message, 'danger');
                },
                onFallbackMode: function () {
                    // When the browser doesn't support this plugin :(
                    ui_add_log('Plugin cant be used here, running Fallback callback');
                },
                onFileSizeError: function (file) {
                    ui_single_update_status(this, 'File excess the size limit', 'danger');

                    ui_add_log('File \'' + file.name + '\' cannot be added: size excess limit');
                },
                onFileTypeError: function (file) {
                    ui_single_update_status(this, 'File type is not an image', 'danger');

                    ui_add_log('File \'' + file.name + '\' cannot be added: must be an image (type error)');
                },
                onFileExtError: function (file) {
                    ui_single_update_status(this, 'File extension not allowed', 'danger');

                    ui_add_log('File \'' + file.name + '\' cannot be added: must be an image (extension error)');
                }
            });
        }

        $(function () {
            setInitdmUploader($('#drag-and-drop-zone1'));
            setInitdmUploader($('#drag-and-drop-zone2'));
            setInitdmUploader($('#drag-and-drop-zone3'));
            setInitdmUploader($('#drag-and-drop-zone4'));
            setInitdmUploader($('#drag-and-drop-zone5'));
            setInitdmUploader($('#drag-and-drop-zone6'));
            setInitdmUploader($('#drag-and-drop-zone7'));
            setInitdmUploader($('#drag-and-drop-zone8'));
            setInitdmUploader($('#drag-and-drop-zone9'));
            setInitdmUploader($('#drag-and-drop-zone10'));
        });
    </script>
@stop

