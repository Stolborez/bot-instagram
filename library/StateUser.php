<?php

use Illuminate\Support\Facades\DB;

class StateUser
{
    public $userId;
    public $name;
    public $surname;
    public $phone;
    public $userName;
    public $chatId;
    public $currentStateId;
    public $prevStateId;

    /**
     * StateUser constructor.
     * @param integer $chat_id
     */
    public function __construct($chat_id)
    {
        $temp = DB::table('bot_users as u')->where('u.chat_id', '=', $chat_id)
            ->leftJoin('state_users_history as uh', 'u.id', '=', 'uh.state_user_id')
            ->leftJoin('state as cs', 'cs.id', '=', 'uh.current_state_id')
            ->leftJoin('state as ps', 'ps.id', '=', 'uh.prev_state_id')
            ->select('u.id as user_id', 'u.username as user_name', 'u.chat_id as chat_id',
                'cs.id as current_state_id', 'ps.id as prev_state_id', 'u.name', 'u.surname', 'u.phone')
            ->orderBy('uh.id', 'desc')
            ->get();
        if (!empty($temp[0])) {
            $this->userId = $temp[0]->user_id;
            $this->name = $temp[0]->name;
            $this->surname = empty($temp[0]->surname) ? '' : $temp[0]->surname;
            $this->phone = empty($temp[0]->phone) ? '' : $temp[0]->phone;
            $this->userName = empty($temp[0]->user_name) ? '' : $temp[0]->user_name;
            $this->chatId = $temp[0]->chat_id;
            $this->currentStateId = $temp[0]->current_state_id;
            $this->prevStateId = $temp[0]->prev_state_id;
        }
    }

    /**
     * Получить название текущего состояния
     * @return string
     */
    public function getCurrentStateCommand()
    {
        $temp = DB::table('state')->where('id', $this->currentStateId)->pluck('code');
        return $temp[0];
    }

    /**
     * Получить название предыдущего состояния
     * @return string
     */
    public function getPrevStateCommand()
    {
        $temp = DB::table('state')->where('id', $this->prevStateId)->pluck('code');
        return $temp[0];
    }

    /**
     * Установить состояние
     * @param $state_code
     */
    public function setState($state_code)
    {
        $this->currentStateId = $this->getStateIdByCommand($state_code);
        if ($state_code == 'start') {
            $this->prevStateId = $this->currentStateId;
        }
    }

    /**
     * Получить ид состояния
     * @param $state_name string Название состояния
     * @return integer
     */
    public function getStateIdByCommand($state_name)
    {
        $temp = DB::table('state')->where('code', $state_name)->pluck('id');
        return $temp[0];
    }

    /**
     * Перейти на следующее состояние
     * @param string $state_name Название следующего состояния
     */
    public function goForward($state_name)
    {
        $next_state_id = $this->getStateIdByCommand($state_name);
        if ($state_name == "start") {
            $this->currentStateId = $next_state_id;
            $this->prevStateId = $next_state_id;
        } else {
            $this->prevStateId = $this->currentStateId;
            $this->currentStateId = $next_state_id;
        }
        $this->save();
    }

    /**
     * Сохранение записи
     */
    public function save()
    {
        $temp = DB::table('bot_users')->where('id', $this->userId)->pluck('id');
        //если пользователь есть в базе
        if (!empty($temp[0])) { // то вставляем в таблицу истории
            DB::table('state_users_history')->insert(['state_user_id' => $this->userId,
                'current_state_id' => $this->currentStateId,
                'prev_state_id' => $this->prevStateId]);
        } else { // иначе сперва вставляем пользователя , а потом в таблицу истории
            DB::transaction(function () {
                $this->userId = $this->guidv4(openssl_random_pseudo_bytes(16));

                DB::table('bot_users')->insert(['id' => $this->userId,
                    'username' => empty($this->userName) ? '' : $this->userName,
                    'name' => $this->name,
                    'surname' => empty($this->surname) ? '' : $this->surname,
                    'phone' => empty($this->phone) ? '' : $this->phone,
                    'chat_id' => $this->chatId
                ]);

                DB::table('state_users_history')->insert(['state_user_id' => $this->userId,
                    'current_state_id' => $this->currentStateId,
                    'prev_state_id' => $this->prevStateId,
                ]);
            });
        }
    }

    // Работа с базой

    public function guidv4($data)
    {
        assert(strlen($data) == 16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    /**
     *предедущий шаг
     */
    public function goBack()
    {
        DB::delete('DELETE FROM state_users_history WHERE id = (SELECT x.id FROM (SELECT MAX(t.id) AS id FROM state_users_history t WHERE t.state_user_id = ?) x)', [$this->userId]);

        $temp = DB::table('bot_users as u')->where('u.chat_id', '=', $this->chatId)
            ->leftJoin('state_users_history as uh', 'u.id', '=', 'uh.state_user_id')
            ->leftJoin('state as cs', 'cs.id', '=', 'uh.current_state_id')
            ->leftJoin('state as ps', 'ps.id', '=', 'uh.prev_state_id')
            ->select('cs.id as current_state_id', 'ps.id as prev_state_id')
            ->orderBy('uh.id', 'desc')
            ->get();
        if (!empty($temp[0])) {
            $this->currentStateId = empty($temp[0]->current_state_id) ? $this->getStateIdByCommand("start") : $temp[0]->current_state_id;
            $this->prevStateId = $temp[0]->prev_state_id ? $this->getStateIdByCommand("start") : $temp[0]->prev_state_id;
        }
    }

    /**
     * Добавление условия в фильтр по параметрам
     * @param $tag_code
     */
    public function addFindParam($tag_code)
    {
        $temp_tag_id = DB::table('tags')->where('code', $tag_code)->pluck('id');
        $temp_exist = DB::table('filter_param_tags')->where('user_id', $this->userId)->where('tag_id', $temp_tag_id[0])->pluck('id');
        if (empty($temp_exist[0])) {
            DB::table('filter_param_tags')->insert(['user_id' => $this->userId, 'tag_id' => $temp_tag_id[0]]);
        }
    }

    /**
     * Добавление условия в фильтр по логике
     * @param $tag_code
     */
    public function addFindLogic($tag_code)
    {
        $temp_tag_id = DB::table('tags')->where('code', $tag_code)->pluck('id');
        $temp_exist = DB::table('filter_logic_tags')->where('user_id', $this->userId)->where('tag_id', $temp_tag_id[0])->pluck('id');
        if (empty($temp_exist[0])) {
            DB::table('filter_logic_tags')->insert(['user_id' => $this->userId, 'tag_id' => $temp_tag_id[0]]);
        }
    }

    /**
     * Добавление модели в корзину
     * @param $model_id
     */
    public function addPurchaseModel($model_id)
    {
        DB::table('purchase')->insert(['id' => $this->guidv4(openssl_random_pseudo_bytes(16)), 'user_id' => $this->userId, 'model_id' => $model_id]);
    }


    /**
     * Удаляет поледние условие фильтра по параметрам
     */
    public function DeleteLastFindParam()
    {
        DB::delete('DELETE FROM filter_param_tags WHERE id = (SELECT x.id FROM (SELECT MAX(t.id) AS id FROM filter_param_tags t WHERE t.user_id = ?) x)', [$this->userId]);
    }

    /**
     * Удаляет условие фильтра по логике
     * @param $tag_code
     */
    public function DeleteFindLogicParam($tag_code)
    {
        DB::table('filter_logic_tags as flt')
            ->join('tags as t', 'flt.tag_id', 't.id')
            ->where('flt.user_id', $this->userId)
            ->where('t.code', $tag_code)
            ->delete();
    }

    /**
     * Удаляет модель из заказа
     * @param $purchaseId
     */
    public function deletePurchase($purchaseId)
    {
        DB::table('purchase')->where('id', $purchaseId)->delete();
        return DB::table('purchase')->where('user_id', $this->userId)->count();
    }

    /**
     * Отправка заказа на сайт
     */
    public function sendPurchase()
    {
        //получаем список корзины
        $purchase = DB::table('purchase')->where('user_id', $this->userId)->get();
        // получаем id статуса нового заказа
        $orderStateId = DB::table('order_stage')->where('stage', '1')->get();

        // генерируем id для вставки записи в таблицу order
        $orderId = $this->guidv4(openssl_random_pseudo_bytes(16));
        //вставляем шапку заказа
        DB::table('order')->insert(['id' => $orderId,
                'code' => date('dmY-His'),
                'user_id' => $this->userId,
                'order_stage_id' => $orderStateId[0]->id]
        );
        // проходим все строки заказа
        foreach ($purchase as $item) {
            $orderLineId = $this->guidv4(openssl_random_pseudo_bytes(16));
            DB::table('order_line')->insert(['id' => $orderLineId,
                'order_id' => $orderId,
                'model_id' => $item->model_id]);
            DB::table('purchase')->where('id', $item->id)->delete();
        }
    }

    /**
     * Отправка заказа на сайт
     */
    public function sendPurchaseConsultant()
    {
        // получаем id статуса нового заказа
        $orderStateId = DB::table('order_stage')->where('stage', '-1')->get();

        // генерируем id для вставки записи в таблицу order
        $orderId = $this->guidv4(openssl_random_pseudo_bytes(16));
        //вставляем шапку заказа
        DB::table('order')->insert(['id' => $orderId,
                'code' => date('dmY-His'),
                'user_id' => $this->userId,
                'order_stage_id' => $orderStateId[0]->id]
        );
    }
}
