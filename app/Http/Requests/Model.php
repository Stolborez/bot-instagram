<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Model extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Получить правила валидации, применимые к запросу.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'surname' => 'required',
            'name' => 'required',
            'patronymic' => 'required',
            'dateofbirth' => 'required',
            'height' => 'required',
            'coloreyes' => 'required',
            'waistmeasurement' => 'required',
            'bustvolume' => 'required',
            'volumehips' => 'required',
            'haircolour' => 'required',
            'footwearsize' => 'required',
            'clothingsize' => 'required',
            'passport' => 'required',
            'departure' => 'required',
            'video' => 'required',
            'workexperience' => 'required',
            'imageLowResolutionUrl' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Заполните поле!',
        ];
    }
}
