<?php

namespace App\Http\Controllers;

use App\Http\Requests\Model;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use RuntimeException;
use Yajra\DataTables\Facades\DataTables;

class ModelController extends Controller
{

    public function getModelsTable()
    {
        $users = DB::table('models')->where('del', 0)->select('id', 'code', 'imageLowResolutionUrl', 'name', 'surname')->get();
        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                return '<button  class="edit-modal btn btn-xs btn-primary" data-info="' . $user->id . '"><i class="glyphicon glyphicon-edit"></i> Редактировать</button>' .
                    '<button class="delete-modal btn btn-xs btn-danger" data-info="' . $user->id . ',' . $user->name . ',' . $user->surname . '"><i class="glyphicon glyphicon-remove"></i> Удалить</button>' .
                    '<br><a href="https://www.instagram.com/p/' . $user->code . '" class="btn btn-xs btn-danger"><i class="fa fa-instagram"></i></i> Instagram</a>';
            })
            ->addColumn('photos', function ($user) {
                return $user->imageLowResolutionUrl;
            })
            ->make(true);
    }

    public function edit(Model $request)
    {
        $temp = ['surname' => $request->surname,
            'name' => $request->name,
            'patronymic' => $request->patronymic,
            'dateofbirth' => (new  DateTime($request->dateofbirth))->format('Y-m-d'),
            'height' => $request->height,
            'coloreyes' => $request->coloreyes,
            'waistmeasurement' => $request->waistmeasurement,
            'bustvolume' => $request->bustvolume,
            'volumehips' => $request->volumehips,
            'haircolour' => $request->haircolour,
            'footwearsize' => $request->footwearsize,
            'clothingsize' => $request->clothingsize,
            'passport' => $request->passport,
            'departure' => $request->departure,
            'video' => $request->video,
            'workexperience' => $request->workexperience,
            'linkInstagram' => $request->linkInstagram,
            'linkvk' => $request->linkvk,
            'imageLowResolutionUrl' => $request->imageLowResolutionUrl,
            'description' => $request->description
        ];
        if (!empty($request->photo_main)) {
            Storage::disk('public_path')->copy('/temp/photos/' . $request->photo_main, '/images/models/photos/' . $request->photo_main);
            Storage::disk('public_path')->delete('/temp/photos/' . $request->photo_main);
            $temp = array_add($temp, 'photo_main', '/images/models/photos/' . $request->photo_main);
        }
        if (!empty($request->photo_1)) {
            Storage::disk('public_path')->copy('/temp/photos/' . $request->photo_1, '/images/models/photos/' . $request->photo_1);
            Storage::disk('public_path')->delete('/temp/photos/' . $request->photo_1);
            $temp = array_add($temp, 'photo_1', '/images/models/photos/' . $request->photo_1);
        }
        if (!empty($request->photo_2)) {
            Storage::disk('public_path')->copy('/temp/photos/' . $request->photo_2, '/images/models/photos/' . $request->photo_2);
            Storage::disk('public_path')->delete('/temp/photos/' . $request->photo_2);
            $temp = array_add($temp, 'photo_2', '/images/models/photos/' . $request->photo_2);
        }
        if (!empty($request->photo_3)) {
            Storage::disk('public_path')->copy('/temp/photos/' . $request->photo_3, '/images/models/photos/' . $request->photo_3);
            Storage::disk('public_path')->delete('/temp/photos/' . $request->photo_3);
            $temp = array_add($temp, 'photo_3', '/images/models/photos/' . $request->photo_3);
        }
        if (!empty($request->photo_4)) {
            Storage::disk('public_path')->copy('/temp/photos/' . $request->photo_4, '/images/models/photos/' . $request->photo_4);
            Storage::disk('public_path')->delete('/temp/photos/' . $request->photo_4);
            $temp = array_add($temp, 'photo_4', '/images/models/photos/' . $request->photo_4);
        }
        if (!empty($request->photo_5)) {
            Storage::disk('public_path')->copy('/temp/photos/' . $request->photo_5, '/images/models/photos/' . $request->photo_5);
            Storage::disk('public_path')->delete('/temp/photos/' . $request->photo_5);
            $temp = array_add($temp, 'photo_5', '/images/models/photos/' . $request->photo_5);
        }
        if (!empty($request->photo_6)) {
            Storage::disk('public_path')->copy('/temp/photos/' . $request->photo_6, '/images/models/photos/' . $request->photo_6);
            Storage::disk('public_path')->delete('/temp/photos/' . $request->photo_6);
            $temp = array_add($temp, 'photo_6', '/images/models/photos/' . $request->photo_6);
        }
        if (!empty($request->photo_7)) {
            Storage::disk('public_path')->copy('/temp/photos/' . $request->photo_7, '/images/models/photos/' . $request->photo_7);
            Storage::disk('public_path')->delete('/temp/photos/' . $request->photo_7);
            $temp = array_add($temp, 'photo_7', '/images/models/photos/' . $request->photo_7);
        }
        if (!empty($request->photo_8)) {
            Storage::disk('public_path')->copy('/temp/photos/' . $request->photo_8, '/images/models/photos/' . $request->photo_8);
            Storage::disk('public_path')->delete('/temp/photos/' . $request->photo_8);
            $temp = array_add($temp, 'photo_8', '/images/models/photos/' . $request->photo_8);
        }
        if (!empty($request->photo_9)) {
            Storage::disk('public_path')->copy('/temp/photos/' . $request->photo_9, '/images/models/photos/' . $request->photo_9);
            Storage::disk('public_path')->delete('/temp/photos/' . $request->photo_9);
            $temp = array_add($temp, 'photo_9', '/images/models/photos/' . $request->photo_9);
        }


        DB::transaction(function () use ($request, $temp) {
            DB::table('models')
                ->where('id', $request->id)
                ->update($temp);

            DB::table('model_tags')
                ->where('model_id', $request->id)
                ->delete();

            if (!empty($request->tags)) {
                foreach ($request->tags as $tag) {
                    DB::table('model_tags')->insert(['id' => $this->guidv4(openssl_random_pseudo_bytes(16)),
                        'model_id' => $request->id,
                        'tag_id' => $tag]);
                }
            }
        });

        $temp = array_add($temp, 'id', $request->id);
        return response()->json($temp);
    }

    public function guidv4($data)
    {
        assert(strlen($data) == 16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    public function delete($id)
    {
        DB::table('models')->where('id', $id)->update(['del' => 1]);
        return response()->json();
    }

//todo удалить
    public function test()
    {
        Storage::disk('public')->copy('/temp/photos/' . '1.png', '/images/models/photos/' . '1.png');
    }

    protected function index()
    {
        $tags = DB::table('tags')->select('id', 'text')->get();
        return view('page.model')->with(['tags' => $tags]);
    }

    protected function getModelData($id)
    {
        $tags = DB::table('model_tags')->where('model_id', $id)->select('tag_id')->get();
        $models = DB::table('models')->where('id', $id)->get();
        $all = array_add($models, 'tags', $tags);
        return json_encode($all, JSON_UNESCAPED_SLASHES);
    }

    protected function uploadPhoto(Request $request)
    {
        try {
            if (!isset($_FILES['file']['error']) || is_array($_FILES['file']['error'])) {
                throw new RuntimeException('Invalid parameters.');
            }
            switch ($_FILES['file']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('Файл не отправлен');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Неизвестная ощибка.');
            }
            $fileName = uniqid() . "_" . $_FILES['file']['name'];
            $filepath = sprintf(public_path('/temp/photos/' . $fileName));
            if (!move_uploaded_file($_FILES['file']['tmp_name'], $filepath)) {
                throw new RuntimeException('Ошибка копирования загруженного файла.');
            }

            // All good, send the response
            return json_encode([
                'status' => 'ok',
                'path' => $fileName
            ]);
        } catch (RuntimeException $e) {
            http_response_code(400);
            return json_encode([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);

        }
    }
}
