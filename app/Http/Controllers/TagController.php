<?php

namespace App\Http\Controllers;

use App\Http\Requests\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class TagController extends Controller
{
    public function getTagsTable()
    {
        $tags = DB::table('tags')->select('id', 'code', 'text')->get();
        return Datatables::of($tags)
            ->addColumn('action', function ($tags) {
                return '<button  class="edit-modal btn btn-xs btn-primary" data-info="' . $tags->id . ',' . $tags->code . ',' . $tags->text . '"><i class="glyphicon glyphicon-edit" ></i> Редактировать</button> ' .
                    '<button class="delete-modal btn btn-xs btn-danger" data-info="' . $tags->id . ',' . $tags->code . ',' . $tags->text . '"><i class="glyphicon glyphicon-trash"></i> Удалить</button>';
            })
            ->make(true);
    }

    public function edit(Tag $request)
    {
        DB::table('tags')
            ->where('id', $request->id)
            ->update(['code' => trim($request->code), 'text' => trim($request->text)]);
        return response()->json(['code' => trim($request->code), 'text' => trim($request->text)]);
    }

    public function delete(Request $request)
    {
        DB::table('tags')->where('id', $request->id)->delete();
        return response()->json();
    }

    public function add(Request $request)
    {
        $id = $this->guidv4(openssl_random_pseudo_bytes(16));
        DB::table('tags')->insert(
            ['id' => $id, 'code' => trim($request->code), 'text' => trim($request->text)]
        );
        return response()->json(['id' => $id, 'code' => trim($request->code), 'text' => trim($request->text)]);
    }

    public function guidv4($data)
    {
        assert(strlen($data) == 16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    protected function index()
    {
        return view('page.tag');
    }
}