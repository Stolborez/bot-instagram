<?php

namespace App\Http\Controllers;

use Illuminate\{
    Support\Facades\DB
};
use StateUser;
use Telegram\{
    Bot\Api, Bot\FileUpload\InputFile, Bot\Keyboard\Keyboard
};


/**
 * Class BotController
 */
class BotController extends Controller
{
    protected $telegram;
    protected $find_emoji;
    protected $find_logic_emoji;
    protected $top10_emoji;
    protected $contacts_emoji;
    protected $insert_emoji;
    protected $state_user;


    /**
     * BotController constructor.
     *
     * @param Api $telegram
     */
    public function __construct(Api $telegram)
    {
        $this->telegram = $telegram;
        $this->find_emoji = "\xF0\x9F\x94\x8D";
        $this->find_logic_emoji = "\xF0\x9F\x99\x8E";
        $this->top10_emoji = "\xF0\x9F\x94\x9D";
        $this->contacts_emoji = "\xE2\x98\x8E";
        $this->insert_emoji = "\xE2\x9E\x95";
    }

    public function test()
    {
        $page = 2;
        $user_id = 'cc82b322-3106-4f97-8783-f3d52d233f70';
        $result_tags = DB::table('filter_param_tags')
            ->Where('user_id', $user_id)
            ->select('tag_id')
            ->get();

        // Формирование постраничного запроса для вывовода моделей
        $result_models = DB::table('models AS m');
        foreach ($result_tags as $tag_id) {
            $result_models = $result_models->whereExists(function ($query) use ($tag_id) {
                $query->select(DB::raw(1))
                    ->from('model_tags AS mt')
                    ->where('mt.tag_id', $tag_id->tag_id)
                    ->whereRaw('mt.model_id = m.id');
            });
        }
        DB::listen(function ($result_models) {
            file_put_contents('sql_log.txt', PHP_EOL . 'sql = ' . $result_models->sql, FILE_APPEND);
            file_put_contents('sql_log.txt', PHP_EOL . 'bindings = ' . json_encode($result_models->bindings), FILE_APPEND);
            file_put_contents('sql_log.txt', PHP_EOL . 'time = ' . $result_models->time, FILE_APPEND);
        });
        file_put_contents('sql_log.txt', PHP_EOL . 'sql = ' . $result_models->toSql(), FILE_APPEND);
        $result_models = $result_models->paginate(1, ['*'], 'pag', $page);
        return dd($result_models[0]->id);
    }

    /**
     * Get updates from Telegram.
     */
    public function me()
    {
        file_put_contents('log.txt', PHP_EOL . ' установлен из колбэка message_id=', FILE_APPEND);
        return $response = $this->telegram->getMe();
    }

    /**
     * Get updates from Telegram.
     */
    public function getUpdates()
    {
        $updates = $this->telegram->getUpdates();
        dd($updates);
        // Do something with the updates
    }


    /**
     * Set a webhook.
     */
    public function setWebhook()
    {
        // Edit this with your webhook URL.
        // You can also use: route('bot-webhook')
        $response = $this->telegram->setWebhook(['url' => 'https://stolborez.s-host.net/public/bot/webhook']);
        dd($response);
    }

    /**
     * Remove webhook.
     *
     */
    public function removeWebhook()
    {
        $response = $this->telegram->removeWebhook();
        return $response;
    }

    /**
     * Handles incoming webhook updates from Telegram.
     *
     * @return string
     */
    public function webhookHandler()
    {
        $update = $this->telegram->getWebhookUpdate();
        $message = $update->getMessage();
        $StateUser = new  StateUser($message->chat->id);

        $current_page = 1;
        $CurrentStateCode = '';
        $messageId = '';

        if ($update->has('callback_query')) {
            if ($update->callbackQuery->data == "back") {
                $StateUser->goBack();
                $CurrentStateCode = $StateUser->getCurrentStateCommand();
            } elseif ($update->callbackQuery->data == 'del_find_param_specify') { //удаление последнего фильтра
                $StateUser->DeleteLastFindParam();
                $StateUser->currentStateId = $StateUser->getStateIdByCommand("find_param");
                $CurrentStateCode = "find_param";
            } elseif ($update->callbackQuery->data == 'purchase_list_send') { // отправка заказа на сайт
                $StateUser->sendPurchase();
                $this->telegram->answerCallbackQuery(['callback_query_id' => $update->callbackQuery->id,
                    'text' => 'Заказ отправлен на обработку. В ближайшее время с вами свяжутся наши менеджеры. ',
                    'show_alert' => true]);
                $StateUser->currentStateId = $StateUser->getStateIdByCommand("menu");
                $CurrentStateCode = "back_to_menu_with_del_mess";
            } elseif (substr($update->callbackQuery->data, 0, 1) == '@') {
                // парсинг параметризированного callbackQuery
                $parse_params = explode("#", $update->callbackQuery->data);
                $parse_state = substr(array_shift($parse_params), 1);
                //Обработка полученных состояний
                switch ($parse_state) {
                    case 'find_param_specify_set'; // добавление условия фильтра по параметрам
                        foreach ($parse_params as $param) {
                            $StateUser->addFindParam('#' . $param);
                        }
                        $StateUser->currentStateId = $StateUser->getStateIdByCommand("find_param");
                        $CurrentStateCode = "find_param";
                        break;
                    case 'find_param_show_questionnaires'; // просмотр модели по параметрам
                        $current_page = array_shift($parse_params);
                        $StateUser->currentStateId = $StateUser->getStateIdByCommand($parse_state);
                        $CurrentStateCode = $parse_state;
                        break;
                    case 'top10'; // просмотр топ 10 моделей
                        $current_page = array_shift($parse_params);
                        $StateUser->currentStateId = $StateUser->getStateIdByCommand($parse_state);
                        $CurrentStateCode = $parse_state;
                        break;
                    case 'purchase_list'; // просмотр корзины
                        $current_page = array_shift($parse_params);
                        $StateUser->currentStateId = $StateUser->getStateIdByCommand($parse_state);
                        $CurrentStateCode = $parse_state;
                        break;
                    case 'del_purchase'; // удаление модели из корзины
                        $purchaseId = array_shift($parse_params);
                        $countPurchase = $StateUser->deletePurchase($purchaseId);
                        file_put_contents('log_my.txt', PHP_EOL . 'countPurchase = ' . $countPurchase, FILE_APPEND);
                        if ($countPurchase == 0) {
                            $StateUser->currentStateId = $StateUser->getStateIdByCommand('menu');
                            $CurrentStateCode = 'back_to_menu_with_del_mess';
                        } else {
                            $StateUser->deletePurchase($purchaseId);
                            $StateUser->currentStateId = $StateUser->getStateIdByCommand('purchase_list');
                            $CurrentStateCode = 'purchase_list';
                        }
                        break;
                    case 'find_logic_show_questionnaires'; // просмотр модели по логике
                        $current_page = array_shift($parse_params);
                        $StateUser->currentStateId = $StateUser->getStateIdByCommand($parse_state);
                        $CurrentStateCode = $parse_state;
                        break;
                    case 'add_purchase'; // добавление модели в корзину
                        $StateUser->addPurchaseModel($parse_params[0]);
                        $this->telegram->answerCallbackQuery(['callback_query_id' => $update->callbackQuery->id,
                            'text' => 'Модель добавлена к корзину. Для оформлениея заказа перейдите в корзину в главном меню. ',
                            'show_alert' => true]);
                        return 'ok';
                        break;
                    case 'find_logic_back';  // возврат к прошлому вопросу по логике и удаление сохраненных фильтров параметров
                        $prev_state = array_shift($parse_params);
                        foreach ($parse_params as $param) {
                            $StateUser->DeleteFindLogicParam('#' . $param);
                        }
                        $StateUser->currentStateId = $StateUser->getStateIdByCommand($prev_state);
                        $CurrentStateCode = $prev_state;
                        break;
                    case 'find_logic_next';  // переход к следующему вопросу по логике и добавление параметров фильтра по логике
                        $next_state = array_shift($parse_params);
                        foreach ($parse_params as $param) {
                            $StateUser->addFindLogic('#' . $param);
                        }
                        $StateUser->currentStateId = $StateUser->getStateIdByCommand($next_state);
                        $CurrentStateCode = $next_state;
                        break;
                }
            } else {
                $StateUser->goForward($update->callbackQuery->data);
                $CurrentStateCode = $StateUser->getCurrentStateCommand();
            }
            $messageId = $update->callbackQuery->message->messageId;
        } else {
            $StateUser->chatId = $message->chat->id;
            $StateUser->name = $message->chat->firstName;
            $StateUser->surname = $message->chat->lastName;
            $StateUser->phone = '';
            $StateUser->userName = $message->chat->username;
            $StateUser->currentStateId = $StateUser->getStateIdByCommand("start");
            $StateUser->prevStateId = $StateUser->getStateIdByCommand("start");
            $StateUser->save();
            $CurrentStateCode = $StateUser->getCurrentStateCommand();
        }

        //  file_put_contents('log_my.txt', PHP_EOL . 'switch = ' . $CurrentStateCode . ' text = ' . $message->text, FILE_APPEND);
        switch ($CurrentStateCode) {
            //region find_param
            case 'find_param';
                $this->findParam($StateUser->chatId, $messageId, $StateUser->userId);
                break;
            case 'find_param_with_del';
                $this->findParamDel($StateUser->chatId, $messageId, $StateUser->userId);
                break;
            case 'find_param_specify';
                $this->findParamSpecify($StateUser->chatId, $messageId);
                break;
            case 'find_param_specify_height';
                $this->findParamHeight($StateUser->chatId, $messageId);
                break;
            case 'find_param_specify_color';
                $this->findParamColor($StateUser->chatId, $messageId);
                break;
            case 'find_param_specify_age';
                $this->findParamAge($StateUser->chatId, $messageId);
                break;
            case 'find_param_specify_sex';
                $this->findParamSex($StateUser->chatId, $messageId);
                break;
            case 'find_param_specify_clothing_size';
                $this->findParamClothingSize($StateUser->chatId, $messageId);
                break;
            case 'find_param_specify_footwear_size';
                $this->findParamFootwearSize($StateUser->chatId, $messageId);
                break;
            case 'find_param_specify_direction';
                $this->findParamDirection($StateUser->chatId, $messageId);
                break;
            case 'find_param_specify_cooperation';
                $this->findParamCooperation($StateUser->chatId, $messageId);
                break;
            case 'find_param_specify_tattoo';
                $this->findParamTattoo($StateUser->chatId, $messageId);
                break;
            case 'find_param_show_questionnaires';
                $this->findParamShowQuestionnaires($StateUser->chatId, $StateUser->userId, $messageId, $current_page);
                break;
            //endregion
            //region find_logic
            case 'find_logic_1';
                $this->findLogic1($StateUser->chatId, $messageId, $StateUser->userId);
                break;
            case 'find_logic_2';
                $this->findLogic2($StateUser->chatId, $messageId);
                break;
            case 'find_logic_3';
                $this->findLogic3($StateUser->chatId, $messageId);
                break;
            case 'find_logic_4';
                $this->findLogic4($StateUser->chatId, $messageId);
                break;
            case 'find_logic_5';
                $this->findLogic5($StateUser->chatId, $messageId);
                break;
            case 'find_logic_6';
                $this->findLogicClothingSize($StateUser->chatId, $messageId);
                break;
            case 'find_logic_clothing_size_more';
                $this->findLogicClothingSizeMore($StateUser->chatId, $messageId);
                break;
            case 'find_logic_show_questionnaires';
                $this->findLogicShowQuestionnaires($StateUser->chatId, $StateUser->userId, $messageId, $current_page);
                break;

            //endregion
            //region purchase_commands
            case 'purchase_list';
                $this->purchaseList($StateUser->chatId, $messageId, $StateUser->userId, $current_page);
                break;
            //endregion
            //region default_commands
            case 'about';
                $this->about($StateUser->chatId, $messageId);
                break;
            case 'insert';
                $this->insert($StateUser->chatId, $messageId);
                break;
            case 'start':
                $this->start($StateUser->chatId);
                break;
            case 'menu':
                $this->showMenu($StateUser->chatId, $messageId);
                break;
            case 'back_to_menu_with_del_mess':
                $this->showMenu($StateUser->chatId, $messageId, false);
                break;
            case 'top10':
                $this->ShowTop10($StateUser->chatId, $messageId, $current_page);
                break;
            case 'contacts';
                $this->showContact($StateUser->chatId, $messageId);
                break;
            case 'consultant';
                $StateUser->sendPurchaseConsultant();
                $this->consultant($StateUser->chatId, $messageId);
                break;
            //endregion
            default:
                $this->start($StateUser->chatId);
        }
        return 'ok';
    }


    private
    function findParam($chatId, $messageId, $uuid_state_user)
    {
        $list_tag_filter = DB::table('filter_param_tags as ftp')
            ->leftJoin('tags as t', 't.id', '=', 'ftp.tag_id')
            ->where('ftp.user_id', $uuid_state_user)
            ->select('t.text')
            ->get();
        $str = '';
        foreach ($list_tag_filter as $item) {
            $str .= $item->text . ',' . chr(10);
        }
        if (empty($str)) {
            $inlineLayout = [
                [Keyboard::inlineButton(['text' => ' Показать анкеты', 'callback_data' => '@find_param_show_questionnaires#1']),],
                [Keyboard::inlineButton(['text' => ' Уточнить параметры модели', 'callback_data' => 'find_param_specify']),],
                [Keyboard::inlineButton(['text' => ' Корзина с заказами', 'callback_data' => 'purchase_list']),],
                [Keyboard::inlineButton(['text' => ' Помощь консультанта', 'callback_data' => 'consultant']),],
                [Keyboard::inlineButton(['text' => ' Назад', 'callback_data' => 'menu']),]
            ];
        } else {
            $inlineLayout = [
                [Keyboard::inlineButton(['text' => ' Показать анкеты', 'callback_data' => '@find_param_show_questionnaires#1']),],
                [Keyboard::inlineButton(['text' => ' Уточнить параметры модели', 'callback_data' => 'find_param_specify']),],
                [Keyboard::inlineButton(['text' => ' Отменить последний выбор', 'callback_data' => 'del_find_param_specify']),],
                [Keyboard::inlineButton(['text' => ' Корзина с заказами', 'callback_data' => 'purchase_list']),],
                [Keyboard::inlineButton(['text' => ' Помощь консультанта', 'callback_data' => 'consultant']),],
                [Keyboard::inlineButton(['text' => ' Назад', 'callback_data' => 'menu']),]
            ];
        }
        $str = substr($str, 0, strlen($str) - 2);
        $text_mess = empty($str) ? 'Подобрать модель по параметрам.' : 'Подобрать модель по параметрам.' . chr(10) . 'Параметры фильтра:' . chr(10) . $str . chr(10);
        $this->editMessageText($chatId, $text_mess, $messageId, $inlineLayout);
    }

    /**
     * Редактирование сообщения
     * @param $chatId
     * @param $textMess
     * @param $messageId
     * @param string $inlineLayout
     */
    private
    function editMessageText($chatId, $textMess, $messageId, $inlineLayout = '')
    {
        $keyboard = '';
        if (!empty($inlineLayout)) {
            $keyboard = Keyboard::make(['inline_keyboard' => $inlineLayout]);
        }

        $this->telegram->editMessageText([
                'chat_id' => $chatId,
                'text' => $textMess,
                'message_id' => $messageId,
                'reply_markup' => $keyboard,]
        );
    }

    private
    function findParamDel($chatId, $messageId, $uuid_state_user)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => ' Показать анкеты', 'callback_data' => '@find_param_show_questionnaires#1']),],
            [Keyboard::inlineButton(['text' => ' Уточнить параметры модели', 'callback_data' => 'find_param_specify']),],
            [Keyboard::inlineButton(['text' => ' Отменить последний выбор', 'callback_data' => 'del_find_param_specify']),],
            [Keyboard::inlineButton(['text' => ' Корзина с заказами', 'callback_data' => 'purchase_list']),],
            [Keyboard::inlineButton(['text' => ' Помощь консультанта', 'callback_data' => 'consultant']),],
            [Keyboard::inlineButton(['text' => ' Назад', 'callback_data' => 'menu']),]
        ];
        $list_tag_filter = DB::table('filter_param_tags as ftp')
            ->leftJoin('tags as t', 't.id', '=', 'ftp.tag_id')
            ->where('ftp.user_id', $uuid_state_user)
            ->select('t.text')
            ->get();
        $str = '';
        foreach ($list_tag_filter as $item) {
            $str .= $item->text . ',' . chr(10);
        }
        $str = substr($str, 0, strlen($str) - 2);
        $text_mess = empty($str) ? 'Подобрать модель по параметрам.' : 'Подобрать модель по параметрам.' . chr(10) . 'Параметры фильтра:' . $str . chr(10);
        $this->telegram->deleteMessage(['chat_id' => $chatId, 'message_id' => $messageId]);
        $this->sendMessage($chatId, $text_mess, $inlineLayout);
    }

    /**
     * Отправить сообщение
     * @param $chatId
     * @param $textMess
     * @param string $inlineLayout
     */
    private
    function sendMessage($chatId, $textMess, $inlineLayout = '')
    {
        $keyboard = '';
        if (!empty($inlineLayout)) {
            $keyboard = Keyboard::make(['inline_keyboard' => $inlineLayout]);
        }
        $this->telegram->sendMessage([
                'chat_id' => $chatId,
                'text' => $textMess,
                'reply_markup' => $keyboard,]
        );
    }

    /**
     * Список параметров фильтра
     * @param $chatId
     * @param $messageId
     */
    private
    function findParamSpecify($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => 'Рост', 'callback_data' => 'find_param_specify_height']),],
            [Keyboard::inlineButton(['text' => 'Цвет волос', 'callback_data' => 'find_param_specify_color']),],
            [Keyboard::inlineButton(['text' => 'Возраст', 'callback_data' => 'find_param_specify_age']),],
            [Keyboard::inlineButton(['text' => 'Пол', 'callback_data' => 'find_param_specify_sex']),],
            [Keyboard::inlineButton(['text' => 'Размер одежды', 'callback_data' => 'find_param_specify_clothing_size']),],
            [Keyboard::inlineButton(['text' => 'Размер обуви', 'callback_data' => 'find_param_specify_footwear_size']),],
            [Keyboard::inlineButton(['text' => 'Уточнить направление', 'callback_data' => 'find_param_specify_direction']),],
            [Keyboard::inlineButton(['text' => 'Условия сотрудничества', 'callback_data' => 'find_param_specify_cooperation']),],
            [Keyboard::inlineButton(['text' => 'Наличие тату/пирсинга', 'callback_data' => 'find_param_specify_tattoo']),],
            [Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => 'back']),]
        ];
        $this->editMessageText($chatId, 'Уточнить параметры поиска:', $messageId, $inlineLayout);
    }

    private
    function findParamHeight($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => 'До 170', 'callback_data' => '@find_param_specify_set#h170']),],
            [Keyboard::inlineButton(['text' => 'От 170', 'callback_data' => '@find_param_specify_set#h171']),],
            [Keyboard::inlineButton(['text' => 'От 175', 'callback_data' => '@find_param_specify_set#h175']),],
        ];
        $this->editMessageText($chatId, 'Уточнить рост', $messageId, $inlineLayout);
    }

    private
    function findParamColor($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => 'Рыжий', 'callback_data' => '@find_param_specify_set#ch4']),],
            [Keyboard::inlineButton(['text' => 'Блондин', 'callback_data' => '@find_param_specify_set#ch1']),],
            [Keyboard::inlineButton(['text' => 'Шатен', 'callback_data' => '@find_param_specify_set#ch3']),],
            [Keyboard::inlineButton(['text' => 'Русый', 'callback_data' => '@find_param_specify_set#ch5']),],
            [Keyboard::inlineButton(['text' => 'Брюнет', 'callback_data' => '@find_param_specify_set#ch2']),],
        ];
        $this->editMessageText($chatId, 'Уточнить цвет волос', $messageId, $inlineLayout);
    }

    private
    function findParamAge($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => 'до 18', 'callback_data' => '@find_param_specify_set#a17']),],
            [Keyboard::inlineButton(['text' => 'от 18', 'callback_data' => '@find_param_specify_set#a18']),],
        ];
        $this->editMessageText($chatId, 'Уточнить возраст', $messageId, $inlineLayout);
    }

    private
    function findParamSex($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => 'М', 'callback_data' => '@find_param_specify_set#m']),],
            [Keyboard::inlineButton(['text' => 'Ж', 'callback_data' => '@find_param_specify_set#f']),],
        ];
        $this->editMessageText($chatId, 'Уточнить пол', $messageId, $inlineLayout);
    }

    private
    function findParamClothingSize($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => 'XXS', 'callback_data' => '#h170']),],
            [Keyboard::inlineButton(['text' => 'XS', 'callback_data' => '#h171']),],
            [Keyboard::inlineButton(['text' => 'S', 'callback_data' => '#h170']),],
            [Keyboard::inlineButton(['text' => 'M', 'callback_data' => '#h171']),],
        ];
        $this->editMessageText($chatId, 'Уточнить размер одежды', $messageId, $inlineLayout);
    }

    private
    function findParamFootwearSize($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => 'XXS', 'callback_data' => '#h170']),],
            [Keyboard::inlineButton(['text' => 'XS', 'callback_data' => '#h171']),],
            [Keyboard::inlineButton(['text' => 'S', 'callback_data' => '#h170']),],
            [Keyboard::inlineButton(['text' => 'M', 'callback_data' => '#h171']),],
        ];
        $this->editMessageText($chatId, 'Уточнить размер одежды', $messageId, $inlineLayout);
    }

    private
    function findParamDirection($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => 'Промо', 'callback_data' => '@find_param_specify_set#pp']),],
            [Keyboard::inlineButton(['text' => 'Подиум', 'callback_data' => '@find_param_specify_set#fw']),],
        ];
        $this->editMessageText($chatId, 'Уточнить направление', $messageId, $inlineLayout);
    }

    private
    function findParamCooperation($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => 'TFP', 'callback_data' => '@find_param_specify_set#tf']),],
            [Keyboard::inlineButton(['text' => 'Коммерция', 'callback_data' => '@find_param_specify_set#cc']),],
        ];
        $this->editMessageText($chatId, 'Уточнить условия сотрудничества', $messageId, $inlineLayout);
    }

    private
    function findParamTattoo($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => 'есть тату/пирсинг', 'callback_data' => '@find_param_specify_set#dnp#cnp']),],
            [Keyboard::inlineButton(['text' => 'Нет тату/пирсинг', 'callback_data' => '@find_param_specify_set#wnp']),],
        ];
        $this->editMessageText($chatId, 'Уточнить наличие тату/пирсинга', $messageId, $inlineLayout);
    }

    /**
     *  Показать анкеты в фильтре по параметрам
     * @param $chatId
     * @param $user_id
     * @param $messageId
     * @param int $page
     */
    private
    function findParamShowQuestionnaires($chatId, $user_id, $messageId, $page = 1)
    {
        // Получение списка тегов выбраных в фильтре
        $result_tags = DB::table('filter_param_tags')
            ->Where('user_id', $user_id)
            ->select('tag_id')
            ->get();

        // Формирование постраничного запроса для вывовода моделей
        $result_models = DB::table('models AS m');
        foreach ($result_tags as $tag_id) {
            $result_models = $result_models->whereExists(function ($query) use ($tag_id) {
                $query->select(DB::raw(1))
                    ->from('model_tags AS mt')
                    ->where('mt.tag_id', $tag_id->tag_id)
                    ->whereRaw('mt.model_id = m.id');
            });
        }

        $result_models = $result_models->paginate(1, ['*'], 'pag', $page);
        if (!empty($result_models[0])) {
            $inlineLayout = [
                [Keyboard::inlineButton(['text' => 'Добавить в корзину', 'callback_data' => '@add_purchase#' . $result_models[0]->id])],
                [Keyboard::inlineButton(['text' => 'Корзина с заказами', 'callback_data' => 'purchase_list']),],
            ];

            array_push($inlineLayout, [Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => 'find_param_with_del'])]);
            array_push($inlineLayout, $this->CreateNavigation($page, $result_models->total(), 'find_param_show_questionnaires'));
            $keyboard = Keyboard::make(['inline_keyboard' => $inlineLayout]);
            // удаляем прошлое сообщение, т.к. сообщение sendPhoto нельзя изменить
            $this->telegram->deleteMessage(['chat_id' => $chatId, 'message_id' => $messageId]);
            $this->telegram->sendPhoto([
                'chat_id' => $chatId,
                'photos' => InputFile::create($result_models[0]->imageLowResolutionUrl),
                'caption' => $result_models[0]->description,
                'reply_markup' => $keyboard
            ]);
        } else {
            $inlineLayout = [
                [Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => 'find_param'])],
            ];
            $this->editMessageText($chatId, 'Моделей по указанным параметрам не найдено. Задайте параметры поиска', $messageId, $inlineLayout);
        }
    }

    /**
     * Вспомогательная функция создания кнопок навигации в просмотре моделей
     * @param $page
     * @param $pages
     * @param $nameState
     * @return array
     */
    private
    function CreateNavigation($page, $pages, $nameState)
    {
        $arr_inline_buttons = [];
        if ($pages > 1) {
            if ($pages <= 5) {
                $start_page = 1;
                $end_page = $pages;
                for ($i = $start_page; $i <= $end_page; $i++) {
                    if ($i == $page) {
                        array_push($arr_inline_buttons, Keyboard::inlineButton(['text' => '.' . $i . '.', 'callback_data' => '@' . $nameState . '#' . $i]));
                    } else
                        array_push($arr_inline_buttons, Keyboard::inlineButton(['text' => $i, 'callback_data' => '@' . $nameState . '#' . $i]));
                }
            } else {
                if (($page - 2) < 1) {
                    $start_page = 1;
                    $end_page = 5;
                    for ($i = $start_page; $i <= $end_page; $i++) {
                        if ($i == $end_page) {
                            array_push($arr_inline_buttons, Keyboard::inlineButton(['text' => $pages . '>>', 'callback_data' => '@' . $nameState . '#' . $pages]));
                        } elseif ($i == $page) {
                            array_push($arr_inline_buttons, Keyboard::inlineButton(['text' => '.' . $i . '.', 'callback_data' => '@' . $nameState . '#' . $i]));
                        } else
                            array_push($arr_inline_buttons, Keyboard::inlineButton(['text' => $i, 'callback_data' => '@' . $nameState . '#' . $i]));
                    }
                } elseif
                (($page + 2) > $pages) {
                    $end_page = $pages;
                    $start_page = $pages - 5;
                    for ($i = $start_page; $i <= $end_page; $i++) {
                        if ($i == $start_page) {
                            array_push($arr_inline_buttons, Keyboard::inlineButton(['text' => '<<' . 1, 'callback_data' => '@' . $nameState . '#' . 1]));
                        } elseif ($i == $page) {
                            array_push($arr_inline_buttons, Keyboard::inlineButton(['text' => '.' . $i . '.', 'callback_data' => '@' . $nameState . '#' . $i]));
                        } else
                            array_push($arr_inline_buttons, Keyboard::inlineButton(['text' => $i, 'callback_data' => '@' . $nameState . '#' . $i]));
                    }

                } else {
                    $start_page = ($page - 2);
                    $end_page = ($page + 2);
                    for ($i = $start_page; $i <= $end_page; $i++) {
                        if ($i == $start_page) {
                            array_push($arr_inline_buttons, Keyboard::inlineButton(['text' => '<<' . 1, 'callback_data' => '@' . $nameState . '#' . 1]));
                        } elseif ($i == $end_page) {
                            array_push($arr_inline_buttons, Keyboard::inlineButton(['text' => $pages . '>>', 'callback_data' => '@' . $nameState . '#' . $pages]));
                        } elseif ($i == $page) {
                            array_push($arr_inline_buttons, Keyboard::inlineButton(['text' => '.' . $i . '.', 'callback_data' => '@' . $nameState . '#' . $i]));
                        } else
                            array_push($arr_inline_buttons, Keyboard::inlineButton(['text' => $i, 'callback_data' => '@' . $nameState . '#' . $i]));
                    }
                }
            }
        }
        return $arr_inline_buttons;
    }

    private
    function findLogic1($chatId, $messageId, $userId)
    {
        DB::table('filter_logic_tags')->where('user_id', $userId)->delete();
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => ' Для подиума', 'callback_data' => '@find_logic_next#find_logic_2#fw']),],
            [Keyboard::inlineButton(['text' => ' Остальные', 'callback_data' => '@find_logic_next#find_logic_2#pp']),],
            [Keyboard::inlineButton(['text' => ' Все', 'callback_data' => '@find_logic_next#find_logic_2']),],
            [Keyboard::inlineButton(['text' => ' Назад', 'callback_data' => 'menu']),],
        ];
        $this->editMessageText($chatId, 'Для подиума или для фото:', $messageId, $inlineLayout);
    }


    private
    function findLogic2($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => ' Мальчик', 'callback_data' => '@find_logic_next#find_logic_3#m']),],
            [Keyboard::inlineButton(['text' => ' Девочка', 'callback_data' => '@find_logic_next#find_logic_3#f']),],
            [Keyboard::inlineButton(['text' => ' Все', 'callback_data' => '@find_logic_next#find_logic_3']),],
            [Keyboard::inlineButton(['text' => ' Вернуться к предыдущему вопросу', 'callback_data' => '@find_logic_back#find_logic_1#fw#pp']),],
        ];
        $this->editMessageText($chatId, 'Мальчик или девочка:', $messageId, $inlineLayout);
    }

    private
    function findLogic3($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => ' до 170', 'callback_data' => '@find_logic_next#find_logic_4#h170']),],
            [Keyboard::inlineButton(['text' => ' от 170', 'callback_data' => '@find_logic_next#find_logic_4#h171']),],
            [Keyboard::inlineButton(['text' => ' от 175', 'callback_data' => '@find_logic_next#find_logic_4#h175']),],
            [Keyboard::inlineButton(['text' => ' Все', 'callback_data' => '@find_logic_next#find_logic_4']),],
            [Keyboard::inlineButton(['text' => ' Вернуться к предыдущему вопросу', 'callback_data' => '@find_logic_back#find_logic_2#m#f']),],
        ];
        $this->editMessageText($chatId, 'Рост:', $messageId, $inlineLayout);
    }

    private
    function findLogic4($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => ' от 18', 'callback_data' => '@find_logic_next#find_logic_5#a18']),],
            [Keyboard::inlineButton(['text' => ' до 18', 'callback_data' => '@find_logic_next#find_logic_5#a17']),],
            [Keyboard::inlineButton(['text' => ' Все', 'callback_data' => '@find_logic_next#find_logic_5']),],
            [Keyboard::inlineButton(['text' => ' Вернуться к предыдущему вопросу', 'callback_data' => '@find_logic_back#find_logic_3#170#171#175']),],
        ];
        $this->editMessageText($chatId, 'Уточнить возраст:', $messageId, $inlineLayout);
    }

    private
    function findLogic5($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => ' TFP', 'callback_data' => '@find_logic_next#find_logic_6#tf']),],
            [Keyboard::inlineButton(['text' => ' Комерция', 'callback_data' => '@find_logic_next#find_logic_6#cc']),],
            [Keyboard::inlineButton(['text' => ' Все', 'callback_data' => '@find_logic_next#find_logic_6']),],
            [Keyboard::inlineButton(['text' => ' Вернуться к предыдущему вопросу', 'callback_data' => '@find_logic_back#find_logic_4#a18#a17']),],
        ];
        $this->editMessageText($chatId, 'Условия сотрудничества:', $messageId, $inlineLayout);
    }

    private
    function findLogicClothingSize($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => 'XXS', 'callback_data' => 'find_logic_clothing_size_more']),],
            [Keyboard::inlineButton(['text' => 'XS', 'callback_data' => 'find_logic_clothing_size_more']),],
            [Keyboard::inlineButton(['text' => 'S', 'callback_data' => 'find_logic_clothing_size_more']),],
            [Keyboard::inlineButton(['text' => 'M', 'callback_data' => 'find_logic_clothing_size_more']),],
            [Keyboard::inlineButton(['text' => ' Вернуться к предыдущему вопросу', 'callback_data' => '@find_logic_back#find_logic_5#tf#cc']),],
        ];
        $this->editMessageText($chatId, 'Размер одежды:', $messageId, $inlineLayout);
    }

    private
    function findLogicClothingSizeMore($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => 'Да', 'callback_data' => 'find_logic_6']),],
            [Keyboard::inlineButton(['text' => 'Нет, показать анкеты', 'callback_data' => '@find_logic_show_questionnaires#1']),],
        ];
        $this->editMessageText($chatId, 'Показать только эти анкеты или добавить еще размеры одежды?', $messageId, $inlineLayout);
    }

    private
    function findLogicShowQuestionnaires($chatId, $user_id, $messageId, $page)
    {
        // Получение списка тегов выбраных в фильтре
        $result_tags = DB::table('filter_logic_tags')
            ->Where('user_id', $user_id)
            ->select('tag_id')
            ->get();

        // Формирование постраничного запроса для вывовода моделей
        $result_models = DB::table('models AS m');
        foreach ($result_tags as $tag_id) {
            $result_models = $result_models->whereExists(function ($query) use ($tag_id) {
                $query->select(DB::raw(1))
                    ->from('model_tags AS mt')
                    ->where('mt.tag_id', $tag_id->tag_id)
                    ->whereRaw('mt.model_id = m.id');
            });
        }

        $result_models = $result_models->paginate(1, ['*'], 'pag', $page);
        if (!empty($result_models[0])) {
            $inlineLayout = [
                [Keyboard::inlineButton(['text' => 'Добавить в корзину', 'callback_data' => '@add_purchase#' . $result_models[0]->id])],
                [Keyboard::inlineButton(['text' => 'Корзина с заказами', 'callback_data' => 'purchase_list']),],
            ];
            array_push($inlineLayout, [Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => 'back_to_menu_with_del_mess'])]);
            array_push($inlineLayout, $this->CreateNavigation($page, $result_models->total(), 'find_param_show_questionnaires'));
            $keyboard = Keyboard::make(['inline_keyboard' => $inlineLayout]);
            // удаляем прошлое сообщение, т.к. сообщение sendPhoto нельзя изменить
            $this->telegram->deleteMessage(['chat_id' => $chatId, 'message_id' => $messageId]);
            $this->telegram->sendPhoto([
                'chat_id' => $chatId,
                'photos' => InputFile::create($result_models[0]->imageLowResolutionUrl),
                'caption' => $result_models[0]->description,
                'reply_markup' => $keyboard
            ]);
        } else {
            $inlineLayout = [
                [Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => 'menu'])],
            ];
            $this->editMessageText($chatId, 'Моделей по указанным параметрам не найдено. Задайте параметры поиска', $messageId, $inlineLayout);
        }
    }

    /**
     * Список заказов
     * @param $chatId
     * @param $messageId
     * @param $userId
     * @param int $page
     */
    private
    function purchaseList($chatId, $messageId, $userId, $page = 1)
    {
        // Формирование постраничного запроса для вывовода моделей
        $resultPurchase = DB::table('purchase as p')
            ->join('bot_users as u', 'u.id', '=', 'p.user_id')
            ->join('models as m', 'p.model_id', '=', 'm.id')
            ->where('user_id', $userId)
            ->select('m.imageLowResolutionUrl', 'p.id', 'm.description');

        $resultPurchase = $resultPurchase->paginate(1, ['*'], 'pag', $page);
        if (!empty($resultPurchase[0])) {
            $inlineLayout = [
                [Keyboard::inlineButton(['text' => ' Удалить из корзины', 'callback_data' => '@del_purchase#' . $resultPurchase[0]->id])],
            ];
            array_push($inlineLayout, [Keyboard::inlineButton(['text' => ' Отправить заказ', 'callback_data' => 'purchase_list_send'])]);
            array_push($inlineLayout, [Keyboard::inlineButton(['text' => ' Назад', 'callback_data' => 'back_to_menu_with_del_mess'])]);
            array_push($inlineLayout, $this->CreateNavigation($page, $resultPurchase->total(), 'purchase_list'));
            $keyboard = Keyboard::make(['inline_keyboard' => $inlineLayout]);
            // удаляем прошлое сообщение, т.к. сообщение sendPhoto нельзя изменить
            $this->telegram->deleteMessage(['chat_id' => $chatId, 'message_id' => $messageId]);
            $this->telegram->sendPhoto([
                'chat_id' => $chatId,
                'photos' => InputFile::create($resultPurchase[0]->imageLowResolutionUrl),
                'caption' => $resultPurchase[0]->description,
                'reply_markup' => $keyboard
            ]);
        } else {
            $inlineLayout = [
                [Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => 'menu'])],
            ];
            $this->editMessageText($chatId, 'Корзина пуста. Добавте моделей', $messageId, $inlineLayout);
        }
    }

    /**
     * Описание организации
     * @param $chatId
     * @param $messageId
     */
    private
    function about($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => 'Помощь консультанта', 'callback_data' => 'consultant']),],
            [Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => 'back']),]
        ];
        $this->editMessageText($chatId, 'SIGMA SCOUTING
СПОНСОР КРАСОТЫ ДЛЯ ВАШЕГО БИЗНЕСА

КРУПНОЕ МЕЖДУНАРОДНОЕ СКАУТИНГОВОЕ АГЕНТСТВО

👩🏻‍💻БОЛЕЕ 1000 | специалистов в команде

🌍 БОЛЕЕ 60 | филиалов в России и странах СНГ

👸🏼БОЛЕЕ 3000 | моделей разного уровня подготовки и типажа

Наша команда активно помогает партнерам получать новых клиентов и увеличивать прибыль. Мы растим профессионалов и привлекаем к работе с партнерами обученных моделей.

Предлагаем вам постоянное сотрудничество и проектную работу. Свяжитесь с нами или оставьте заявку нашему консультанту.', $messageId, $inlineLayout);
    }

    private
    function insert($chatId, $messageId)
    {
        $inlineLayout = [

            [Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => 'back']),]
        ];
        $this->editMessageText($chatId, 'Вставить в базу', $messageId, $inlineLayout);
    }

    /**
     * Стартовое сообщение
     * @param $chatId
     */
    private
    function start($chatId)
    {
        $inlineLayout = [
            [
                Keyboard::inlineButton(['text' => 'Показать меню', 'callback_data' => 'back_to_menu_with_del_mess']),
            ]
        ];
        $keyboard = Keyboard::make(['inline_keyboard' => $inlineLayout]);
        $this->telegram->sendPhoto([
            'chat_id' => $chatId,
            'photos' => InputFile::create(__DIR__ . '/inm.jpg'),
            'caption' => 'Приветствую вас!
            
Вам нужны модели на показ, фотосессию или проект?

Я БОТ-скаут «Sigma Scouting». Моя задача - помочь Вам подобрать моделей из нашей базы.',
            'reply_markup' => $keyboard
        ]);
        // $this->sendMessage($chatId, 'Приветствие', $inlineLayout);
    }

    /**
     *  Главное меню
     * @param $chatId
     * @param $messageId
     * @param bool $flagAction
     */
    public
    function showMenu($chatId, $messageId, $flagAction = true)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => ' Подобрать модель по параметрам', 'callback_data' => 'find_param']),],
            [Keyboard::inlineButton(['text' => ' Подобрать модель по логике', 'callback_data' => 'find_logic_1']),],
            [Keyboard::inlineButton(['text' => 'Топ-10 моделей', 'callback_data' => 'top10']),],
            // [Keyboard::inlineButton(['text' => json_decode('"' . $this->insert_emoji . '"') . ' Встать в базу', 'callback_data' => 'insert']),],
            [Keyboard::inlineButton(['text' => ' Корзина с заказами', 'callback_data' => 'purchase_list']),],
            [Keyboard::inlineButton(['text' => ' Помощь консультанта', 'callback_data' => 'consultant']),],
            [Keyboard::inlineButton(['text' => ' Сотрудничество', 'callback_data' => 'about']),],
            [Keyboard::inlineButton(['text' => ' Контакты и обратная связь', 'callback_data' => 'contacts']),],
        ];
        if ($flagAction) {
            $this->editMessageText($chatId, 'Выберите пунк меню:', $messageId, $inlineLayout);
        } else {
            $this->telegram->deleteMessage(['chat_id' => $chatId, 'message_id' => $messageId]);
            $this->sendMessage($chatId, 'Выберите пунк меню:', $inlineLayout);
        }
    }

    /**
     *  Показать топ 10
     * @param $chatId
     * @param $messageId
     * @param int $page
     */
    private
    function ShowTop10($chatId, $messageId, $page = 1)
    {
        // Формирование постраничного запроса для вывовода моделей
        $result_models = DB::table('models as m')
            ->join('model_tags as tm', 'tm.model_id', '=', 'm.id')
            ->join('tags as t', 't.id', '=', 'tm.tag_id')
            ->where('t.CODE', '#top10');

        $result_models = $result_models->paginate(1, ['*'], 'pag', $page);
        if (!empty($result_models[0])) {
            $inlineLayout = [
                [Keyboard::inlineButton(['text' => 'Добавить в корзину', 'callback_data' => '@add_purchase#' . $result_models[0]->id])],
                [Keyboard::inlineButton(['text' => 'Корзина с заказами', 'callback_data' => 'purchase_list']),],
            ];
            array_push($inlineLayout, [Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => 'back_to_menu_with_del_mess'])]);
            array_push($inlineLayout, $this->CreateNavigation($page, $result_models->total(), 'top10'));
            $keyboard = Keyboard::make(['inline_keyboard' => $inlineLayout]);
            // удаляем прошлое сообщение, т.к. сообщение sendPhoto нельзя изменить
            $this->telegram->deleteMessage(['chat_id' => $chatId, 'message_id' => $messageId]);
            $this->telegram->sendPhoto([
                'chat_id' => $chatId,
                'photos' => InputFile::create($result_models[0]->imageLowResolutionUrl),
                'caption' => $result_models[0]->description,
                'reply_markup' => $keyboard
            ]);
        } else {
            $inlineLayout = [
                [Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => 'menu'])],
            ];
            $this->editMessageText($chatId, 'Моделей по указанным параметрам не найдено. Задайте параметры поиска', $messageId, $inlineLayout);
        }


        $this->editMessageText($chatId, 'Вывод топ 10', $messageId, $inlineLayout);
    }

    /**
     * Показать контакты
     * @param $chatId
     * @param $messageId
     */
    private
    function showContact($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => 'Помощь консультанта', 'callback_data' => 'consultant']),],
            [Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => 'back']),],
        ];
        $this->editMessageText($chatId, 'Горячая линия РФ:
8 800 250 07 82

Горячая линия РБ:
+375 (17) 200-35-20

Звонки принимаются с 12:00 до 21:00 
по московскому времени. 
Суббота – выходной.

В РФ ООО \'\'Сигма\'\' ИНН7842122490 

В РБ ООО \'\'Сигма Моделс\'\' УНП192939494

ВСЕ ПРАВА ЗАЩИЩЕНЫ © 2015-2018', $messageId, $inlineLayout);
    }

    /**
     * Связаться с консультантом
     * @param $chatId
     * @param $messageId
     */
    private
    function consultant($chatId, $messageId)
    {
        $inlineLayout = [
            [Keyboard::inlineButton(['text' => 'Назад', 'callback_data' => 'back'])],
        ];
        $this->editMessageText($chatId, 'Консультатн свяжется с вами в ближайшее время', $messageId, $inlineLayout);
    }
}