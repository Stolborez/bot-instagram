<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use InstagramScraper\Instagram;

class ParserController extends Controller
{
    protected function parse()
    {
        $instagram = Instagram::withCredentials('stolborez', '1WolB6r7NuQi');
        $medias = $instagram->getMedias("sigma_model", 3000);
        for ($i = 0; $i < count($medias) - 1; $i++) {
            $media = $medias[$i];
            $text_tag = $user = strstr($media->getCaption(), '#');
            $temp_model = DB::table('models')->where('code', $media->getShortCode())->pluck('id');
            if (!empty($temp_model[0])) {
                DB::table('models')
                    ->where('code', $media->getShortCode())
                    ->where('blocked', false)
                    ->update(['imageLowResolutionUrl' => $media->getImageLowResolutionUrl(),
                        'imageStandardResolutionUrl' => $media->getImageStandardResolutionUrl(),
                        'imageHighResolutionUrl' => $media->getImageHighResolutionUrl(),
                        'code' => $media->getShortCode(),
                        'fullcaption' => $media->getCaption()
                    ]);
                // Очистка таблицы тегов для модели
                DB::table('model_tags')->where('model_id', $temp_model[0])->delete();
            } else {
                $temp_model[0] = $this->guidv4(openssl_random_pseudo_bytes(16));
                DB::table('models')->insert(
                    ['id' => $temp_model[0],
                        'imageLowResolutionUrl' => $media->getImageLowResolutionUrl(),
                        'imageStandardResolutionUrl' => $media->getImageStandardResolutionUrl(),
                        'imageHighResolutionUrl' => $media->getImageHighResolutionUrl(),
                        'code' => $media->getShortCode(),
                        'fullcaption' => $media->getCaption(),
                        'blocked' => false,
                    ]
                );
                echo $temp_model[0] . '<br>';
            }

            foreach (explode("#", substr($text_tag, 1)) as $tag) {
                $temp_tag = DB::table('tags')->where('code', "#" . trim($tag))->pluck('id');
                if (!empty($temp_tag[0])) {
                    DB::table('model_tags')->insert(['id' => $this->guidv4(openssl_random_pseudo_bytes(16)),
                        'model_id' => $temp_model[0],
                        'tag_id' => $temp_tag[0]
                    ]);
                } else {
                    echo "Не найден тег:" . $tag . '<br>';
                }
            }
        }
    }

    protected function guidv4($data)
    {
        assert(strlen($data) == 16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    protected function index()
    {
        return view('page.parse');
    }
}

