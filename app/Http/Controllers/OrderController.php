<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class OrderController extends Controller
{
    public function getOrdersTable()
    {
        $orders = DB::table('order as o')
            ->join('bot_users as u', 'u.id', 'o.user_id')
            ->join('order_stage as os', 'os.id', 'o.order_stage_id')
            ->select('o.*', 'u.name', 'u.chat_id', 'os.name as name_stage')
            ->get();

        return Datatables::of($orders)
            ->addColumn('action', function ($orders) {
                return '<button class="delete-modal btn btn-xs btn-danger" data-info="' . $orders->id . ',' . $orders->code . '"><i class="glyphicon glyphicon-trash"></i> Удалить</button>';
            })
            ->addColumn('link', function ($orders) {
                $order_list = DB::table('order_line as ol')
                    ->join('models as m', 'm.id', 'ol.model_id')
                    ->where('order_id', $orders->id)
                    ->select('m.code', 'm.imageLowResolutionUrl')
                    ->get();
                $orderList = '<table  border="0" style="padding-left:150px;width: 100%;">';
                foreach ($order_list as $item) {
                    $orderList .=
                        '<tr>' .
                        '<td><img src=' . $item->imageLowResolutionUrl . ' border="0" width="40" align="center" /></td>' .
                        '<td>' . $item->code . '</td>' .
                        '<td>' .
                        '<a href="https://www.instagram.com/p/' . $item->code . '" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-"></i>Instagram</a>' .
                        '</td>' .
                        '</tr>';
                }
                return $orderList . '</table>';
            })->rawColumns(['action', 'link'])
            ->make(true);
    }


    public function delete(Request $request)
    {
        DB::table('order_line')->where('order_id', $request->id)->delete();
        DB::table('order')->where('id', $request->id)->delete();
        return response()->json();
    }


    protected function index()
    {
        return view('page.order');
    }
}