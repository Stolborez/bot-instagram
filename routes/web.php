<?php

/*
|--------------------------------------------------------------------------
| Manual Updates Route
|--------------------------------------------------------------------------
| This route is used to fetch bot updates manually from Telegram,
|
| NOTE: THIS WILL NOT WORK IF WEBHOOK IS ENABLED.
*/

Route::get('/bot/updates', 'BotController@getUpdates')->name('bot-updates');
/*
|--------------------------------------------------------------------------
| Set Webhook
|--------------------------------------------------------------------------
| This route is used to set your webhook with Telegram,
| just request from your browser once and then disable it.
|
| Example: http://domain.com/bot/set-webhook
*/
Route::get('/bot/set-webhook', 'BotController@setWebhook')->name('bot-set-webhook');
/*
|--------------------------------------------------------------------------
| Remove Webhook
|--------------------------------------------------------------------------
| This route is used to remove your webhook with Telegram,
| just request from your browser once and then disable it.
|
| Example: http://domain.com/bot/remove-webhook
*/
Route::get('/bot/remove-webhook', 'BotController@removeWebhook')->name('bot-remove-webhook');
/*
|--------------------------------------------------------------------------
| Webhook (Incoming Handler)
|--------------------------------------------------------------------------
| This route handles incoming webhook updates.
|
| !! IMPORTANT !!
| THIS IS AN INSECURE ENDPOINT FOR DEMO PURPOSES,
| MAKE SURE TO SECURE THIS ENDPOINT, EXAMPLE: "/bot/<SECRET-KEY>/webhook"
|
| THEN SET THAT WEBHOOK WITH TELEGRAM.
| SO YOU CAN BE SURE THE UPDATES ARE COMING FROM TELEGRAM ONLY.
*/
Route::post('/bot/webhook', 'BotController@webhookHandler')->name('bot-webhook');

Route::get('bot/me', 'BotController@me')->name('bot-me');
Route::get('bot/test', 'BotController@test')->name('bot-test');

Route::get('/', 'ModelController@index');


/*
|--------------------------------------------------------------------------
| Маршруты ModelController
|--------------------------------------------------------------------------
*/
/*
Отображение страницы моделей
*/
Route::get('/models', 'ModelController@index');

/*
Получение данных для таблицы моделей
*/
Route::get('/get/models', 'ModelController@getModelsTable');
/*
Редактирование данных модели
*/
Route::post('/edit/model', 'ModelController@edit');
/*
Редактирование данных модели
*/
Route::post('/get/model/{id}', 'ModelController@getModelData');
/*
Пометка данных модели на удаление
*/
Route::post('/del/model/{id}', 'ModelController@delete');
/*
Загрузка фото
*/
Route::post('/model/photos', 'ModelController@uploadPhoto');
//todo удалить
/*
test
*/
Route::get('/testm', 'ModelController@test');

/*
|--------------------------------------------------------------------------
| Маршруты TagController
|--------------------------------------------------------------------------
*/
/*
Отображение страницы тегов
*/
Route::get('/tags', 'TagController@index');
/*
Получение данных для таблицы тегов
*/
Route::get('/get/tags', 'TagController@getTagsTable');
/*
Редактирование данных тега
*/
Route::post('/edit/tag', 'TagController@edit');
/*
Удаление тега
*/
Route::post('/delete/tag', 'TagController@delete');
/*
Добавление тега
*/
Route::post('/add/tag', 'TagController@add');

/*
|--------------------------------------------------------------------------
| Маршруты OrderController
|--------------------------------------------------------------------------
*/
/*
Отображение страницы тегов
*/
Route::get('/orders', 'OrderController@index');
/*
//Получение данных для таблицы тегов
*/
Route::get('/get/orders', 'OrderController@getOrdersTable');
/*
Удаление тега
*/
Route::post('/delete/order', 'OrderController@delete');

/*
|--------------------------------------------------------------------------
| Маршруты ParserController
|--------------------------------------------------------------------------
*/
/*
Парсинг моделей с instagram
*/
Route::get('parse/get', 'ParserController@parse')->name('parse');
/*
Парсинг моделей с instagram
*/
Route::get('/parse', 'ParserController@index');
